﻿#include "sysusermessage.h"

SysUserMessage::SysUserMessage()
{
    this->tableName = QString("func_sys_user_message");
    this->title = nullptr;
    this->content = nullptr;
    this->type = nullptr;
    this->readStatus = nullptr;
    this->deleteStatus = nullptr;
    this->sysMessageId = nullptr;
    this->userId = nullptr;
    this->publicTime.setTime_t(0);
}

QStringList SysUserMessage::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "title";
    fieldList << "content";
    fieldList << "type";
    fieldList << "read_status";
    fieldList << "delete_status";
    fieldList << "sys_message_id";
    fieldList << "user_id";
    fieldList << "public_time";
    return fieldList;
}

void SysUserMessage::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->title = valueMap["title"].toString();
    this->content = valueMap["content"].toString();
    this->type = valueMap["type"].toString();
    this->readStatus = valueMap["read_status"].toString();
    this->deleteStatus = valueMap["delete_status"].toString();
    this->sysMessageId = valueMap["sys_message_id"].toString();
    this->userId = valueMap["user_id"].toString();
    this->publicTime = valueMap["public_time"].toDateTime();
}

QString SysUserMessage::getUpdateString()
{
    QStringList updateList = getUpdateStringList();
    if(title != nullptr)
        updateList << QString("title='%1'").arg(title);
    if(content != nullptr)
        updateList << QString("content='%1'").arg(content);
    if(type != nullptr)
        updateList << QString("type='%1'").arg(type);
    if(readStatus != nullptr)
        updateList << QString("read_status='%1'").arg(readStatus);
    if(deleteStatus != nullptr)
        updateList << QString("delete_status='%1'").arg(deleteStatus);
    if(sysMessageId != nullptr)
        updateList << QString("sys_message_id=%1").arg(sysMessageId);
    if(userId != nullptr)
        updateList << QString("user_id=%1").arg(userId);
    if(publicTime.toTime_t() > 0)
        updateList << QString("public_time='%1'").arg(publicTime.toString("yyyy-MM-dd hh:mm:ss"));

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(updateList.join(",")).arg(id.toInt());
    return sql;
}

QString SysUserMessage::getInsertString()
{
    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(title != nullptr)
    {
        fieldList << "title";
        valueList << "'" + title + "'";
    }
    if(content != nullptr)
    {
        fieldList << "content";
        valueList << "'" + content + "'";
    }
    if(type != nullptr)
    {
        fieldList << "type";
        valueList << "'" + type + "'";
    }
    if(readStatus != nullptr)
    {
        fieldList << "read_status";
        valueList << "'" + readStatus + "'";
    }
    if(deleteStatus != nullptr)
    {
        fieldList << "delete_status";
        valueList << "'" + deleteStatus + "'";
    }

    if(sysMessageId != nullptr)
    {
        fieldList << "sys_message_id";
        valueList << sysMessageId;
    }
    if(userId != nullptr)
    {
        fieldList << "user_id";
        valueList << userId;
    }
    if(publicTime.toTime_t() > 0)
    {
        fieldList << "public_time";
        valueList << "'" + publicTime.toString("yyyy-MM-dd hh:mm:ss") + "'";
    }
    if(fieldList.size() == 0)
        return nullptr;

    QString sql1 = fieldList.join(",");
    QString sql2 = valueList.join(",");
    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(sql1)
            .arg(sql2);
    return sql;
}


QJsonObject SysUserMessage::toJson()
{
    QJsonObject obj;
    obj.insert("id",         getId());
    obj.insert("version",    getVersion());
    obj.insert("createName", getCreateName());
    obj.insert("createDate", getCreateDate().toMSecsSinceEpoch());
    obj.insert("updateName", getUpdateName());
    obj.insert("updateDate", getUpdateDate().toMSecsSinceEpoch());

    obj.insert("title",       getTitle());
    obj.insert("content",     getContent());
    obj.insert("type",        getType());
    obj.insert("readStatus",  getReadStatus());
    obj.insert("deleteStatus",getDeleteStatus());
    obj.insert("sysMessageId",getSysMessageId());
    obj.insert("userId",      getUserId());
    obj.insert("publicTime",  getPublicTime().toMSecsSinceEpoch());

    return obj;
}


