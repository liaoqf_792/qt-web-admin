﻿#include "sysrolemenu.h"

SysRoleMenu::SysRoleMenu()
{    
    this->tableName = QString("sys_role_menu");
    this->roleId = nullptr;
    this->menuId = nullptr;
}

QStringList SysRoleMenu::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "role_id";
    fieldList << "menu_id";
    return fieldList;
}
void SysRoleMenu::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->roleId = valueMap["role_id"].toString();
    this->menuId = valueMap["menu_id"].toString();
}
QString SysRoleMenu::getUpdateString()
{
    QStringList sqlSetList = getUpdateStringList();
    if(roleId != nullptr)
        sqlSetList << QString("role_id=%1").arg(roleId);
    if(menuId != nullptr)
        sqlSetList << QString("menu_id=%1").arg(menuId);

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(sqlSetList.join(",")).arg(id.toInt());
    return sql;

}
QString SysRoleMenu::getInsertString()
{

    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(roleId != nullptr)
    {
        fieldList << "role_id";
        valueList << roleId;
    }
    if(menuId != nullptr)
    {
        fieldList << "menu_id";
        valueList << menuId;
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(fieldList.join(","))
            .arg(valueList.join(","));
    return sql;
}
QJsonObject SysRoleMenu::toJson()
{
    QJsonObject obj;
    obj.insert("id",         getId());
    obj.insert("version",    getVersion());
    obj.insert("createName", getCreateName());
    obj.insert("createDate", getCreateDate().toMSecsSinceEpoch());
    obj.insert("updateName", getUpdateName());
    obj.insert("updateDate", getUpdateDate().toMSecsSinceEpoch());

    obj.insert("roleId",     getRoleId());
    obj.insert("menuId",     getMenuId());

    return obj;
}
