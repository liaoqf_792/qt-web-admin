﻿#ifndef SYSUSERROLE_H
#define SYSUSERROLE_H


#include "tableentity.h"
class SysUserRole : public TableEntity
{
public:
    SysUserRole();

    QStringList getFieldList();
    void        setData(QMap<QString, QVariant>& valueMap);
    QString     getUpdateString();
    QString     getInsertString();
    QJsonObject toJson();

    //setter getter
    int     getUserId()   {return userId.toInt();}
    int     getRoleId()   {return roleId.toInt();}

    void setUserId(int userId){this->userId = QString::number(userId);}
    void setRoleId(int roleId){this->roleId = QString::number(roleId);}


private:
    QString userId;   //用户id
    QString roleId;   //角色id
};

#endif // SYSUSERROLE_H
