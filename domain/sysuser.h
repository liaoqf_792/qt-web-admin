﻿#ifndef SYSUSER_H
#define SYSUSER_H

#include "tableentity.h"

class SysUser :public TableEntity
{
public:
    SysUser();

    QStringList getFieldList();
    void setData(QMap<QString, QVariant>& valueMap);
    QString getUpdateString();
    QString getInsertString();

    //setter getter    
    QString getUserName(){return userName;}
    QString getRealName(){return realName;}
    QString getPassword(){return password;}
    QString getSalt(){return salt;}
    QString getHeadImgUrl(){return headImgUrl;}
    QString getStatus(){return status;}
    QString getSex(){return sex;}
    int     getDeptId(){return deptId.toInt();}
    QString getEmail(){return email;}
    QString getTelNo(){return telNo;}
    QString getUserNo(){return userNo;}
    QString getMemo(){return memo;}

    void setUserName(QString userName){this->userName = userName;}
    void setRealName(QString realName){this->realName = realName;}
    void setPassword(QString password){this->password = password;}
    void setSalt(QString salt){this->salt = salt;}
    void setHeadImgUrl(QString headImgUrl){this->headImgUrl = headImgUrl;}
    void setStatus(QString status){this->status = status;}
    void setSex(QString sex){this->sex = sex;}
    void setDeptId(int deptId){this->deptId = QString::number(deptId);}
    void setEmail(QString email){this->email = email;}
    void setTelNo(QString telNo){ this->telNo = telNo;}
    void setUserNo(QString userNo){this->userNo = userNo;}
    void setMemo(QString memo){this->memo = memo;}

private:

    QString userName;   //用户名
    QString realName;   //真实姓名
    QString password;   //密码
    QString salt;       //盐
    QString headImgUrl; //头像
    QString status;     //状态
    QString sex;        //性别
    QString deptId;     //所属部门
    QString email;      //邮箱
    QString telNo;      //手机号
    QString userNo;     //用户编号
    QString memo;       //备注
};

#endif // SYSUSER_H
