﻿#ifndef TABLEENTITY_H
#define TABLEENTITY_H

#include <QMap>
#include <QSet>
#include <QVariant>
#include <QDateTime>
#include <QJsonObject>


class TableEntity
{
public:
    TableEntity();

    //获取更新SQL语句
    virtual QString getUpdateString() = 0;
    //获取插入SQL语句
    virtual QString getInsertString() = 0;
    //获取插入字段列表
    virtual QStringList getFieldList()= 0;
    //保存查询结果到实体
    virtual void setData(QMap<QString, QVariant>& valueMap) = 0;

    //getter setter
    void setId(int id) {this->id = QString::number(id);}
    void setVersion(int version) {this->version = QString::number(version);}
    void setCreateName(QString createName) {this->createName = createName;}
    void setCreateDate(QDateTime createDate) {this->createDate = createDate;}
    void setUpdateName(QString updateName) {this->updateName = updateName;}
    void setUpdateDate(QDateTime updateDate) {this->updateDate = updateDate;}
    void setTableName(QString tableName){this->tableName = tableName;}

    int       getId(){ return id.toInt();}
    int       getVersion(){ return version.toInt();}
    QString   getCreateName(){ return createName;}
    QDateTime getCreateDate(){ return createDate;}
    QString   getUpdateName(){ return updateName;}
    QDateTime getUpdateDate(){ return updateDate;}
    QString   getTableName(){ return tableName;}

protected:
    //获取默认6个字段的更新SQL语句
    QStringList getUpdateStringList();
    //获取默认6个字段
    QStringList getEntityFieldList();
    //获取默认6个字段的插入SQL语句
    void getEntityInsertString(QStringList& fields, QStringList& values);
    //保存默认6个字段查询结果到实体
    void setEntityData(QMap<QString, QVariant>& valueMap);

    QString tableName;      //表名

    QString id;            //id
    QString version;       //版本号
    QString createName;     //创建人
    QDateTime createDate;   //创建时间
    QString updateName;     //更新人
    QDateTime updateDate;   //更新时间

};

#endif // TABLEENTITY_H
