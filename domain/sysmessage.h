﻿#ifndef SYSMESSAGE_H
#define SYSMESSAGE_H

#include "tableentity.h"

class SysMessage: public TableEntity
{
public:
    //消息类型（0：系统消息，1：其他消息）
    static const int Type_System = 0;
    static const int Type_Other = 1;
    //消息状态（0：草稿，1：已发布）
    static const int Status_Draft = 0;
    static const int Status_Publish = 1;

    SysMessage();

    QStringList getFieldList();
    void        setData(QMap<QString, QVariant>& valueMap);
    QString     getUpdateString();
    QString     getInsertString();
    QJsonObject toJson();

    //setter getter
    QString   getTitle()         {return title;}
    QString   getContent()      {return content;}
    QString   getType()         {return type;}
    QString   getStatus()       {return status;}
    QDateTime getPublicTime()   {return publicTime;}

    void setTitle(QString title)                {this->title = title;}
    void setContent(QString content)            {this->content = content;}
    void setType(QString type)                  {this->type = type;}
    void setStatus(QString status)              {this->status = status;}
    void setPublicTime(QDateTime publicTime)    {this->publicTime = publicTime;}

private:

    QString   title;         //消息标题
    QString   content;       //内容
    QString   type;          //消息类型
    QString   status;        //消息状态
    QDateTime publicTime;    //发布时间

};

#endif // SYSMESSAGE_H
