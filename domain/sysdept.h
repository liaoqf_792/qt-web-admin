﻿#ifndef SYSDEPT_H
#define SYSDEPT_H

#include "tableentity.h"

class SysDept:public TableEntity
{
public:
    SysDept();

    QStringList getFieldList();
    void        setData(QMap<QString, QVariant>& valueMap);
    QString     getUpdateString();
    QString     getInsertString();
    QJsonObject toJson();

    //setter getter
    QString getDeptNo()     {return deptNo;}
    QString getName()       {return name;}
    QString getShortName()  {return shortName;}
    int     getParentId()   {return parentId.toInt();}
    QString getType()       {return type;}
    int     getOrderNo()    {return orderNo.toInt();}
    QString getMemo()       {return memo;}

    void setDeptNo(QString deptNo){this->deptNo = deptNo;}
    void setName(QString name){this->name = name;}
    void setShortName(QString shortName){this->shortName = shortName;}
    void setParentId(int parentId){this->parentId = QString::number(parentId);}
    void setType(QString type){this->type = type;}
    void setOrderNo(int orderNo){this->orderNo = QString::number(orderNo);}
    void setMemo(QString memo){this->memo = memo;}

private:

    QString deptNo;     //部门编号
    QString name;       //部门名称
    QString shortName;  //部门简称
    QString parentId;  //上级部门
    QString type;       //部门类型
    QString orderNo;   //排序
    QString memo;       //备注
};

#endif // SYSDEPT_H
