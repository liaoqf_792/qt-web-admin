﻿#ifndef SYSDICENTITY_H
#define SYSDICENTITY_H

#include "tableentity.h"

class SysDictionary:public TableEntity
{
public:
    SysDictionary();

    QStringList getFieldList();
    void setData(QMap<QString, QVariant>& valueMap);
    QString getUpdateString();
    QString getInsertString();


    QString getCode(){return code;}
    QString getName(){return name;}
    QString getMemo(){return memo;}
    void setCode(QString code){this->code = code;}
    void setName(QString name){this->name = name;}
    void setMemo(QString memo){this->memo = memo;}

private:
    QString code;      //字典代码
    QString name;      //字段名称
    QString memo;      //备注

};

#endif // SYSDICENTITY_H
