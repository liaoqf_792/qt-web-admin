﻿#ifndef SYSUSERMESSAGE_H
#define SYSUSERMESSAGE_H

#include "tableentity.h"

class SysUserMessage: public TableEntity
{
public:

    //消息是否阅读（0：未读，1：已读）
    static const int Read_No = 0;
    static const int Read_Yes = 1;
    //消息是否删除（0：未删除，1：已删除）
    static const int Delete_No = 0;
    static const int Delete_Yes = 1;

    SysUserMessage();

    QStringList getFieldList();
    void        setData(QMap<QString, QVariant>& valueMap);
    QString     getUpdateString();
    QString     getInsertString();
    QJsonObject toJson();

    //setter getter
    QString   getTitle()        {return title;}
    QString   getContent()      {return content;}
    QString   getType()         {return type;}
    QString   getReadStatus()   {return readStatus;}
    QString   getDeleteStatus() {return deleteStatus;}
    int       getSysMessageId() {return sysMessageId.toInt();}
    int       getUserId()       {return userId.toInt();}
    QDateTime getPublicTime()   {return publicTime;}

    void setTitle(QString title)                {this->title = title;}
    void setContent(QString content)            {this->content = content;}
    void setType(QString type)                  {this->type = type;}
    void setReadStatus(QString readStatus)      {this->readStatus = readStatus;}
    void setDeleteStatus(QString deleteStatus)  {this->deleteStatus = deleteStatus;}
    void setSysMessageId(int sysMessageId)      {this->sysMessageId = QString::number(sysMessageId);}
    void setUserId(int userId)                  {this->userId = QString::number(userId);}
    void setPublicTime(QDateTime publicTime)    {this->publicTime = publicTime;}

private:
    QString   title;         //消息标题
    QString   content;       //内容
    QString   type;          //消息类型
    QString   readStatus;    //阅读状态
    QString   deleteStatus;  //删除状态
    QString   sysMessageId;  //系统消息id
    QString   userId;        //用户id
    QDateTime publicTime;    //发布时间
};

#endif // SYSUSERMESSAGE_H
