﻿#include "tableentity.h"

TableEntity::TableEntity()
{    

    this->tableName = nullptr;
    this->id = nullptr;
    this->version = nullptr;
    this->createName = nullptr;
    this->createDate.setTime_t(0);
    this->updateName = nullptr;
    this->updateDate.setTime_t(0);
}


QStringList TableEntity::getEntityFieldList()
{
    QStringList fieldList;
    fieldList << "id";
    fieldList << "version";
    fieldList << "create_name";
    fieldList << "create_date";
    fieldList << "update_name";
    fieldList << "update_date";
    return fieldList;
}


QStringList TableEntity::getUpdateStringList()
{
    QStringList updateList;
    if(version != nullptr)
        updateList << QString("version=%1").arg(version);
    if(createName != nullptr)
        updateList << QString("create_name='%1'").arg(createName);
    if(createDate.toTime_t() > 0)
        updateList << QString("create_date='%1'").arg(createDate.toString("yyyy-MM-dd hh:mm:ss"));
    if(updateName != nullptr)
        updateList << QString("update_name='%1'").arg(updateName);
    if(updateDate.toTime_t() > 0)
        updateList << QString("update_date='%1'").arg(updateDate.toString("yyyy-MM-dd hh:mm:ss"));
    return updateList;
}

void TableEntity::setEntityData(QMap<QString, QVariant>& valueMap)
{
    this->id = valueMap["id"].toString();
    this->version = valueMap["version"].toString();
    this->createName = valueMap["create_name"].toString();
    this->createDate = valueMap["create_date"].toDateTime();
    this->updateName = valueMap["update_name"].toString();
    this->updateDate = valueMap["update_date"].toDateTime();
}


void TableEntity::getEntityInsertString(QStringList& fieldList, QStringList& valueList)
{
    if(version != nullptr)
    {
        fieldList << "version";
        valueList << version;
    }
    if(createName != nullptr)
    {
        fieldList << "create_name";
        valueList << "'" + createName + "'";
    }
    if(createDate.toTime_t() > 0)
    {
        fieldList << "create_date";
        valueList << "'" + createDate.toString("yyyy-MM-dd hh:mm:ss") + "'";
    }
    if(updateName != nullptr)
    {
        fieldList << "update_name";
        valueList << "'" + updateName + "'";
    }
    if(updateDate.toTime_t() > 0)
    {
        fieldList << "update_date";
        valueList << "'" + updateDate.toString("yyyy-MM-dd hh:mm:ss") + "'";
    }
}
