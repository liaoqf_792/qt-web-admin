﻿#ifndef SYSMENU_H
#define SYSMENU_H


#include "tableentity.h"

class SysMenu : public TableEntity
{
public:
    SysMenu();

    QStringList getFieldList();
    void        setData(QMap<QString, QVariant>& valueMap);
    QString     getUpdateString();
    QString     getInsertString();
    QJsonObject toJson();

    //setter getter
    QString getName()       {return name;}
    int     getParentId()   {return parentId.toInt();}
    QString getType()       {return type;}
    QString getUrl()        {return url;}
    QString getIcon()       {return icon;}
    int     getOrderNo()    {return orderNo.toInt();}
    QString getMemo()       {return memo;}

    void setName(QString name){this->name = name;}
    void setParentId(int parentId){this->parentId = QString::number(parentId);}
    void setType(QString type){this->type = type;}
    void setUrl(QString url){this->url = url;}
    void setIcon(QString icon){this->icon = icon;}
    void setOrderNo(int orderNo){this->orderNo = QString::number(orderNo);}
    void setMemo(QString memo){this->memo = memo;}

private:

    QString name;       //菜单名称
    QString parentId;   //父级菜单ID
    QString type;       //菜单类型  0：菜单 1： 按钮
    QString url;        //菜单URL
    QString icon;       //菜单图标
    QString orderNo;    //菜单排序
    QString memo;       //备注
};

#endif // SYSMENU_H
