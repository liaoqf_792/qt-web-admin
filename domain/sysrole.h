﻿#ifndef SYSROLE_H
#define SYSROLE_H

#include "tableentity.h"

class SysRole : public TableEntity
{
public:
    SysRole();

    QStringList getFieldList();
    void        setData(QMap<QString, QVariant>& valueMap);
    QString     getUpdateString();
    QString     getInsertString();
    QJsonObject toJson();

    //setter getter
    QString getName()       {return name;}
    int     getParentId()   {return parentId.toInt();}
    QString getRoleNo()     {return roleNo;}
    int     getOrderNo()    {return orderNo.toInt();}
    QString getMemo()       {return memo;}

    void setName(QString name){this->name = name;}
    void setParentId(int parentId){this->parentId = QString::number(parentId);}
    void setRoleNo(QString roleNo){this->roleNo = roleNo;}
    void setOrderNo(int orderNo){this->orderNo = QString::number(orderNo);}
    void setMemo(QString memo){this->memo = memo;}

private:

    QString name;       //角色名称
    QString parentId;   //父角色id
    QString roleNo;     //角色编号
    QString orderNo;    //菜单排序
    QString memo;       //备注


};

#endif // SYSROLE_H
