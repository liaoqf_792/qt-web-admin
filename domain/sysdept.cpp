﻿#include "sysdept.h"

SysDept::SysDept()
{
    this->tableName = QString("sys_depart");
    this->deptNo = nullptr;
    this->name = nullptr;
    this->shortName = nullptr;
    this->parentId = nullptr;
    this->type = nullptr;
    this->orderNo = nullptr;
    this->memo = nullptr;
}

void SysDept::setData(QMap<QString, QVariant>& valueMap)
{
    TableEntity::setEntityData(valueMap);
    this->deptNo = valueMap["dept_no"].toString();
    this->name = valueMap["name"].toString();
    this->shortName = valueMap["short_name"].toString();
    this->parentId = valueMap["parent_id"].toString();
    this->type = valueMap["type"].toString();
    this->orderNo = valueMap["order_no"].toString();
    this->memo = valueMap["memo"].toString();
}

QStringList SysDept::getFieldList()
{
    QStringList fieldList = TableEntity::getEntityFieldList();
    fieldList << "dept_no";
    fieldList << "name";
    fieldList << "short_name";
    fieldList << "parent_id";
    fieldList << "type";
    fieldList << "order_no";
    fieldList << "memo";
    return fieldList;
}

QString SysDept::getUpdateString()
{
    QStringList sqlSetList = getUpdateStringList();
    if(deptNo != nullptr)
        sqlSetList << QString("dept_no='%1'").arg(deptNo);
    if(name != nullptr)
        sqlSetList << QString("name='%1'").arg(name);
    if(shortName != nullptr)
        sqlSetList << QString("short_name='%1'").arg(shortName);
    if(parentId != nullptr)
        sqlSetList << QString("parent_id='%1'").arg(parentId);
    if(type != nullptr)
        sqlSetList << QString("type='%1'").arg(type);
    if(orderNo != nullptr)
        sqlSetList << QString("order_no=%'").arg(orderNo);
    if(memo != nullptr)
        sqlSetList << QString("memo='%1'").arg(memo);

    QString sql = QString("update %1 set %2 where id=%3")
            .arg(getTableName()).arg(sqlSetList.join(",")).arg(id.toInt());
    return sql;
}

QString SysDept::getInsertString()
{
    QStringList fieldList;
    QStringList valueList;
    TableEntity::getEntityInsertString(fieldList, valueList);

    if(deptNo != nullptr)
    {
        fieldList << "dept_no";
        valueList << "'" + deptNo + "'";
    }
    if(name != nullptr)
    {
        fieldList << "name";
        valueList << "'" + name + "'";
    }
    if(shortName != nullptr)
    {
        fieldList << "short_name";
        valueList << "'" + shortName + "'";
    }
    if(parentId != nullptr)
    {
        fieldList << "parent_id";
        valueList << parentId;
    }
    if(type != nullptr)
    {
        fieldList << "type";
        valueList << "'" + type + "'";
    }
    if(orderNo != nullptr)
    {
        fieldList << "order_no";
        valueList << orderNo;
    }
    if(memo != nullptr)
    {
        fieldList << "memo";
        valueList << "'" + memo + "'";
    }

    if(fieldList.size() == 0)
        return nullptr;

    QString sql = QString("insert into %1 (%2) values (%3)")
            .arg(getTableName())
            .arg(fieldList.join(","))
            .arg(valueList.join(","));
    return sql;
}

QJsonObject SysDept::toJson()
{
    QJsonObject obj;
    obj.insert("id",         getId());
    obj.insert("version",    getVersion());
    obj.insert("createName", getCreateName());
    obj.insert("createDate", getCreateDate().toMSecsSinceEpoch());
    obj.insert("updateName", getUpdateName());
    obj.insert("updateDate", getUpdateDate().toMSecsSinceEpoch());

    obj.insert("deptNo",     getDeptNo());
    obj.insert("name",       getName());
    obj.insert("shortName",  getShortName());
    obj.insert("parentId",   getParentId());
    obj.insert("type",       getType());
    obj.insert("orderNo",    getOrderNo());
    obj.insert("memo",       getMemo());

    return obj;
}
