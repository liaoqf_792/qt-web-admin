﻿#ifndef SYSROLEMENU_H
#define SYSROLEMENU_H

#include "tableentity.h"
class SysRoleMenu : public TableEntity
{
public:
    SysRoleMenu();

    QStringList getFieldList();
    void        setData(QMap<QString, QVariant>& valueMap);
    QString     getUpdateString();
    QString     getInsertString();
    QJsonObject toJson();

    //setter getter
    int     getRoleId()   {return roleId.toInt();}
    int     getMenuId()   {return menuId.toInt();}

    void setRoleId(int roleId){this->roleId = QString::number(roleId);}
    void setMenuId(int menuId){this->menuId = QString::number(menuId);}


private:

    QString roleId;   //角色id
    QString menuId;   //角色id
};

#endif // SYSROLEMENU_H
