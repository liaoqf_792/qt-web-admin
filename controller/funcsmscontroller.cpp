﻿#include "funcsmscontroller.h"

FuncSmsController::FuncSmsController()
{
    m_pHelper = DbMgr::instance()->getHelper();
    m_mapFunc.insert("/func/sms/qcloudsms",          &FuncSmsController::qcloudsms);
    m_mapFunc.insert("/func/sms/sendqcloudsms",      &FuncSmsController::sendQcloudSms);
    m_mapFunc.insert("/func/sms/alisms",             &FuncSmsController::alisms);
    m_mapFunc.insert("/func/sms/sendalisms",         &FuncSmsController::sendAliSms);

}

void FuncSmsController::service(HttpRequest& request, HttpResponse& response)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString access_token = request.getParameter("access_token");
    QString username = JwtUtil::getUsername(access_token);
    if(username == NULL || username.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }
    SysUser visitUser;
    if(!m_pHelper->selectUserByName(visitUser, username))
    {
        ret.failedMsg(QStringLiteral("用户不存在，操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QByteArray path = request.getPath().toLower();
    if(m_mapFunc.contains(path))
    {
        pServFunc func = m_mapFunc.value(path);
        (this->*func)(request, response, visitUser);
    }
}

//Request URL: http://localhost:9005/func/sms/qcloudsms
//{"code":0,"data":{"appid":"1403024316","appkey":"123","smsSign":"湖北测试","templateId":"494564"},"msg":""}
void FuncSmsController::qcloudsms(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QJsonObject obj;
    obj.insert("appid", "1403024316");
    obj.insert("appkey", "123");
    obj.insert("smsSign", QStringLiteral("湖北测试"));
    obj.insert("templateId", "SMS_180046878");

    ResultJson ret(0, true, QStringLiteral("操作成功"));
    ret.setData(obj);
    ResponseUtil::replyJson(response, ret);
}

//Request URL: http://localhost:9004/demo/func/sms/sendQcloudSms
/*
appid: 1403024316
appkey: 123
smsSign: 湖北测试
templateId: 494564
phoneNo: 13332222222
access_token: ?
{"code":0,"success":true,"msg":"短信提交成功, 是否能接受到短信，还需要运营商返回状态","data":null}
*/
void FuncSmsController::sendQcloudSms(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QString appid = request.getParameter("appid");
    QString smsSign = request.getParameter("smsSign");
    QString templateId = request.getParameter("templateId");
    QString phoneNo = request.getParameter("phoneNo");

    ResultJson ret(0, true, QStringLiteral("短信提交成功, 是否能接受到短信，还需要运营商返回状态"));
    ResponseUtil::replyJson(response, ret);
}



//Request URL: http://localhost:9005/func/sms/alisms
//{"code":0,"data":{"accessKeyId":"123","accessSecret":"123","regionId":"cn-hangzhou","smsSign":"湖北测试","templateCode":"SMS_180046878"},"msg":""}
void FuncSmsController::alisms(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QJsonObject obj;
    obj.insert("accessKeyId", "123");
    obj.insert("accessSecret", "123");
    obj.insert("regionId", "cn-hangzhou");
    obj.insert("smsSign", QStringLiteral("湖北测试"));
    obj.insert("templateCode", "SMS_180046878");

    ResultJson ret(0, true, QStringLiteral("操作成功"));
    ret.setData(obj);
    ResponseUtil::replyJson(response, ret);
}
//Request URL: http://localhost:9005/func/sms/sendAliSms
/*
regionId: cn-hangzhou
accessKeyId: 123
accessSecret: 123
smsSign: 湖北测试
templateCode: SMS_180046878
phoneNo: 13255558888
access_token: ?
{"code":0,"success":true,"msg":"短信提交成功, 是否能接受到短信，还需要运营商返回状态","data":null}
*/
void FuncSmsController::sendAliSms(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QString regionId = request.getParameter("regionId");
    QString accessKeyId = request.getParameter("accessKeyId");
    QString accessSecret = request.getParameter("accessSecret");
    QString smsSign = request.getParameter("smsSign");
    QString templateCode = request.getParameter("templateCode");
    QString phoneNo = request.getParameter("phoneNo");

    ResultJson ret(0, true, QStringLiteral("短信提交成功, 是否能接受到短信，还需要运营商返回状态"));
    ResponseUtil::replyJson(response, ret);
}
