﻿#include "funcemailcontroller.h"

FuncEmailController::FuncEmailController()
{    
    m_pHelper = DbMgr::instance()->getHelper();
    m_mapFunc.insert("/sys/email/send",  &FuncEmailController::send);
}

void FuncEmailController::service(HttpRequest& request, HttpResponse& response)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString access_token = request.getParameter("access_token");
    QString username = JwtUtil::getUsername(access_token);
    if(username == NULL || username.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }
    SysUser visitUser;
    if(!m_pHelper->selectUserByName(visitUser, username))
    {
        ret.failedMsg(QStringLiteral("用户不存在，操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QByteArray path = request.getPath().toLower();
    if(m_mapFunc.contains(path))
    {
        pServFunc func = m_mapFunc.value(path);
        (this->*func)(request, response, visitUser);
    }
}

//http://localhost:9005/sys/email/send
/*
mailbox: 333
subject: 222
content: 33
access_token: ?
*/
//{"code":0,"success":false,"msg":"邮件发送失败","data":null}
void FuncEmailController::send(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QString mailbox = request.getParameter("mailbox");
    QString subject = request.getParameter("subject");
    QString content = request.getParameter("content");


    ResultJson ret(0, false, QStringLiteral("邮件发送失败"));
    ResponseUtil::replyJson(response, ret);
}
