﻿#ifndef SYSMENUCONTROLLER_H
#define SYSMENUCONTROLLER_H


#include "global.h"
#include <QMap>

using namespace stefanfrings;

class SysMenuController: public HttpRequestHandler {
    Q_OBJECT
    Q_DISABLE_COPY(SysMenuController)

public:
    SysMenuController();
    void    service(HttpRequest& request, HttpResponse& response);
    void       list(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //获取权限菜单
    void   menuType(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //获取菜单类型
    void   menuName(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //获取菜单名称
    void   datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //获取菜单列表
    void       save(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //保存菜单信息
    void    delMenu(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //删除菜单信息

private:
    SqlHelper* m_pHelper;
    typedef void (SysMenuController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;

    QJsonArray getMenuChildren(SysMenu& menu, QList<SysMenu> menuList); //获取菜单层级关系
};

#endif // SYSMENUCONTROLLER_H
