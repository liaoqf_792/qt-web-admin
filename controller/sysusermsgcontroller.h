﻿#ifndef SYSUSERMSGCONTROLLER_H
#define SYSUSERMSGCONTROLLER_H


#include "global.h"

using namespace stefanfrings;
class SysUserMsgController: public HttpRequestHandler {
    Q_OBJECT
    Q_DISABLE_COPY(SysUserMsgController)

public:
    SysUserMsgController();
    void  service(HttpRequest& request, HttpResponse& response);
    void   newmsg(HttpRequest& request, HttpResponse& response, SysUser& visitUser); //获取用户未读消息
    void   detail(HttpRequest& request, HttpResponse& response, SysUser& visitUser); //获取用户消息明细
    void datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser); //获取用户消息列表
    void      del(HttpRequest& request, HttpResponse& response, SysUser& visitUser); //删除用户系统消息
    void     read(HttpRequest& request, HttpResponse& response, SysUser& visitUser); //已读用户消息
    void  readAll(HttpRequest& request, HttpResponse& response, SysUser& visitUser); //全部已读用户消息

private:
    SqlHelper* m_pHelper;
    typedef void (SysUserMsgController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSUSERMSGCONTROLLER_H
