﻿#ifndef SERVERMONITORCONTROLLER_H
#define SERVERMONITORCONTROLLER_H

#include "global.h"
#include <QMap>
using namespace stefanfrings;

class FuncMonitorController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(FuncMonitorController)
public:
    FuncMonitorController();
    void    service(HttpRequest& request, HttpResponse& response);
    void     server(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //页面获取服务器信息
private:
    SqlHelper* m_pHelper;
    typedef void (FuncMonitorController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SERVERMONITORCONTROLLER_H
