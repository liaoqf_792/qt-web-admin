﻿#ifndef SYSLOGCONTROLLER_H
#define SYSLOGCONTROLLER_H

#include "global.h"
#include <QMap>

using namespace stefanfrings;

class SysLogController: public HttpRequestHandler {
    Q_OBJECT
    Q_DISABLE_COPY(SysLogController)
public:
    SysLogController();
    void    service(HttpRequest& request, HttpResponse& response);
    void    logType(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //获取日志类型
    void   datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser);  //获取系统日志列表

private:
    SqlHelper* m_pHelper;
    typedef void (SysLogController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSLOGCONTROLLER_H
