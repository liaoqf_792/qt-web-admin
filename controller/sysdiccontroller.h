﻿#ifndef SYSDICCONTROLLER_H
#define SYSDICCONTROLLER_H

//字典管理
#include "global.h"
#include <QMap>

class SysDicController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(SysDicController)

public:
    SysDicController();
    void    service(HttpRequest& request, HttpResponse& response);
    void     getDic(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取字典
    void   datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取数据字典列表
    void       save(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //保存数据字典
    void    saveVal(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //保存字典值
    void     delDic(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //删除字典


private:
    SqlHelper* m_pHelper;
    typedef void (SysDicController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSDICCONTROLLER_H
