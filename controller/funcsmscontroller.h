﻿#ifndef SYSSMSCONTROLLER_H
#define SYSSMSCONTROLLER_H

#include "global.h"
#include <QMap>
using namespace stefanfrings;

class FuncSmsController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(FuncSmsController)
public:
    FuncSmsController();
    void            service(HttpRequest& request, HttpResponse& response);
    void          qcloudsms(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void      sendQcloudSms(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void             alisms(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    void         sendAliSms(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
private:
    SqlHelper* m_pHelper;
    typedef void (FuncSmsController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSSMSCONTROLLER_H
