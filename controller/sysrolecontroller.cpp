﻿#include "sysrolecontroller.h"

SysRoleController::SysRoleController()
{

    m_pHelper = DbMgr::instance()->getHelper();
    m_mapFunc.insert("/sys/role/setmenu",      &SysRoleController::setMenu);     //获取权限树形数据
    m_mapFunc.insert("/sys/role/rolename",     &SysRoleController::getRoleName); //获取角色名称
    m_mapFunc.insert("/sys/role/datagrid",     &SysRoleController::datagrid);    //获取角色列表
    m_mapFunc.insert("/sys/role/save",         &SysRoleController::save);        //保存角色信息
    m_mapFunc.insert("/sys/role/delete",       &SysRoleController::delRole);     //删除角色信息
    m_mapFunc.insert("/sys/role/saverolemenu", &SysRoleController::saveRoleMenu);//保存权限菜单
}

void SysRoleController::service(HttpRequest& request, HttpResponse& response)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString access_token = request.getParameter("access_token");
    QString username = JwtUtil::getUsername(access_token);
    if(username == NULL || username.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }
    SysUser visitUser;
    if(!m_pHelper->selectUserByName(visitUser, username))
    {
        ret.failedMsg(QStringLiteral("用户不存在，操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QByteArray path = request.getPath().toLower();
    if(m_mapFunc.contains(path))
    {
        pServFunc func = m_mapFunc.value(path);
        (this->*func)(request, response, visitUser);
    }
}

//获取权限树形数据
//http://localhost:9004/sys/role/setMenu?roleId=?&access_token=?
//{"code":0,"data":[{"checkArr":[{"isChecked":"0","type":"0"}],"id":"36df3e985a7c01432cc579728425bf09","parentId":"0","title":"项目介绍"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"87c8451f951888e483fe7ec7668507fe","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"栅格"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"0d079901d30c6bdbcae1349de1aabd30","parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","title":"功能示例一"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"391f39b83b80cc3bd68e115d935f8109","parentId":"f9679bc12890ed472d0fec02dd3fad64","title":"功能演示一"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"3b362bf507f42d306368728df86877e0","parentId":"76cf30bf5db55a9204e38dec11079e2a","title":"功能演示一"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"426c5207e1f95db3ca968ec508aea490","parentId":"a03f6123a7a1be65e514edd28de0ae8d","title":"基本资料"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"4f8fa64d0ec7fce7c2cedc5ac6d09489","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"邮件发送"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"59f249d45a9478beae170b4a4ce03b61","parentId":"254d304678038641f87aff42be68ef74","title":"简单数据表格"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"623262eac50915905031d8d67d8702ea","parentId":"0","title":"系统管理"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"700bb6c4a8d5cd6b9d7016e4faae8628","parentId":"36df3e985a7c01432cc579728425bf09","title":"项目主页"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"755956ba2ea812d8749a062f34b7da2d","parentId":"623262eac50915905031d8d67d8702ea","title":"用户管理"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"75e8d5210cf7c50af4db8f560c020974","parentId":"7de00ce887ea0c9c7144636112801a0c","title":"新增"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"84cc6a4dbda00fc1d3a24d55c9cd14e6","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"按钮"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"a7194554d65798806bac6732d36f67a4","parentId":"524ab0ea6d1bd06ea8f7430acf258b41","title":"功能演示"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"aa04121cc3d2e79286103acc24c35770","parentId":"755956ba2ea812d8749a062f34b7da2d","title":"新增"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ae1dd4b597a544660759211fa1481b1b","parentId":"6ee28a2031c8d1e701613082ea6ebc5e","title":"新增"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ae6e226f18c93b6fc93c1b6817242741","parentId":"a9adea345ac1afeb3292e576bbfec804","title":"表单元素"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"be09904bbab6e8ad917bf6bf2d3c6ce7","parentId":"213830032d2b72edf575f30d71361a51","title":"定时任务"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"c98b436c0bdf3e1db2aacbbaa6fcaca4","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"新增字典"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"d4f0ee03c841e22d7cb1cbd271c63100","parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","title":"layedit 编辑器"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"da49ca0fe2ea8be4bb557c251c6180c3","parentId":"7476783545496331c211296efaa561a2","title":"druid 监控"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"fe70cf2fa1985a461186efcaf4633283","parentId":"87c8451f951888e483fe7ec7668507fe","title":"等比例列表排列"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ff358bb4ca406915664f8cb8b848e9da","parentId":"ecacd9efd6c237b80e46df8b1507a904","title":"新增"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"19587742caad08e1c5eaa9483b02f49b","parentId":"755956ba2ea812d8749a062f34b7da2d","title":"保存"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"1fc380fb7a2b3992e5f0dce39a683ce3","parentId":"28d1d2a835dc364315cc7f9832d664a0","title":"腾讯云短信"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"4b2e9da372c5c7b5d4d39c0f9a4e402d","parentId":"7476783545496331c211296efaa561a2","title":"服务器监控"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"6ee28a2031c8d1e701613082ea6ebc5e","parentId":"623262eac50915905031d8d67d8702ea","title":"部门管理"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"73c52763764317a58bb2b7db884e8788","parentId":"f9679bc12890ed472d0fec02dd3fad64","title":"功能演示二"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"7476783545496331c211296efaa561a2","parentId":"0","title":"系统监控"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"7e106236ac4833f27ffa0250d3492071","parentId":"36df3e985a7c01432cc579728425bf09","title":"示例主页一"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"83b3db7973133104ba24135b453939ce","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"新增字典值"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"9e278b9a4ee34914d1dd1969e800eb67","parentId":"76cf30bf5db55a9204e38dec11079e2a","title":"功能演示二"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ac698e2709fdcb90ee8b9ed88e5905d1","parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","title":"功能示例二"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"adc1f1a492ac837bfec6292265c3b30d","parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","title":"kz.layedit 编辑器"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"af52b09768353133a4d3aab4f167d4c4","parentId":"ecacd9efd6c237b80e46df8b1507a904","title":"保存"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"b44ad72e6203953c8f6a74521d05c8aa","parentId":"a03f6123a7a1be65e514edd28de0ae8d","title":"修改密码"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"b7eadf3fdfe67c7ff3223c838213a584","parentId":"524ab0ea6d1bd06ea8f7430acf258b41","title":"特殊示例"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"be00c02c463f91af6433918bee8219c4","parentId":"a9adea345ac1afeb3292e576bbfec804","title":"表单组合"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"c9f52990bc82165f63e45df34077896c","parentId":"7de00ce887ea0c9c7144636112801a0c","title":"保存角色"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"d080eec50903b82466fe1aa19f78b01d","parentId":"6ee28a2031c8d1e701613082ea6ebc5e","title":"保存"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ecf0f6a8f0a34d0587695c5351c073d3","parentId":"213830032d2b72edf575f30d71361a51","title":"任务日志"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ee2ca956201793d7f035d2ae6e944c93","parentId":"254d304678038641f87aff42be68ef74","title":"列宽自动分配"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"f4bc6809cec463e0206deb80319dfb97","parentId":"87c8451f951888e483fe7ec7668507fe","title":"按移动端排列"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"0537fa19001c1758fe1d7e8ae93cb61a","parentId":"28d1d2a835dc364315cc7f9832d664a0","title":"阿里云短信"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"06428eaef123e23852946540710eafe7","parentId":"524ab0ea6d1bd06ea8f7430acf258b41","title":"风格定制"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"0f5a97c074f42292c477c1ecb8e58769","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"保存字典"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"1b52228fe7c8b9cf259f9f7c9bfcd844","parentId":"254d304678038641f87aff42be68ef74","title":"赋值已知数据"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"207ae629bfbf53290f3c64f1933d5ae6","parentId":"36df3e985a7c01432cc579728425bf09","title":"示例主页二"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"28d1d2a835dc364315cc7f9832d664a0","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"短信服务"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"3d0d3eed4985181a82a6387a30974a9c","parentId":"28d1d2a835dc364315cc7f9832d664a0","title":"云潮云短信"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"427d532a84547250e510a3dcc388c92f","parentId":"6ee28a2031c8d1e701613082ea6ebc5e","title":"删除"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"4388d13e00a1a94300c5fa7ff32d9d90","parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","title":"tinymce 编辑器"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"6aa4271aff8644eeb1561f57f8320446","parentId":"755956ba2ea812d8749a062f34b7da2d","title":"删除"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"7bf0c837223ed4b2693eadc933d5de56","parentId":"ecacd9efd6c237b80e46df8b1507a904","title":"删除"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"7de00ce887ea0c9c7144636112801a0c","parentId":"623262eac50915905031d8d67d8702ea","title":"角色管理"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"7e3a22591b016b3ce6ccc881e7b9d294","parentId":"7de00ce887ea0c9c7144636112801a0c","title":"保存权限"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"8eec007fbd23c0a318ba3e74f9c3d2a9","parentId":"87c8451f951888e483fe7ec7668507fe","title":"移动桌面端组合"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"91be39ac09cdc71c7435ffb94f611795","parentId":"0","title":"常用功能"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"a9adea345ac1afeb3292e576bbfec804","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"表单"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ab83dd17afdebc0ffe0d20318e23ea07","parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","title":"设定主题"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"213830032d2b72edf575f30d71361a51","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"定时任务"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"33ad4150fead0f227d7c6adb3d4fde2f","parentId":"36df3e985a7c01432cc579728425bf09","title":"示例主页三"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"40cad6aa986f987303647287352f7788","parentId":"87c8451f951888e483fe7ec7668507fe","title":"全端复杂组合"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"4618aa278da605c57895074aee4e095d","parentId":"254d304678038641f87aff42be68ef74","title":"转化静态表格"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"612265d33877f681c26b2180f90a3e62","parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","title":"特殊示例"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"a5efa8ea44340e69710c398838c78989","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"保存字典值"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"bf5b7351e458a15a1ad7cf4421eb9493","parentId":"7de00ce887ea0c9c7144636112801a0c","title":"删除"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ecacd9efd6c237b80e46df8b1507a904","parentId":"623262eac50915905031d8d67d8702ea","title":"菜单管理"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ff544ef46b64157f10b8c20a3ae33d0d","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"导航"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"2774f4f1c30a34a75dfcfe5f8945d370","parentId":"0","title":"示例组件"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"2f9b5ee346d667a5c146b3a25009bc8c","parentId":"623262eac50915905031d8d67d8702ea","title":"日志管理"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"3e5df1746ba7a3be358e41cedff4c825","parentId":"3c02556f5054c9fdf6faf6574004e792","title":"删除"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"54c7509f7a0fca385c935e4919dc10e7","parentId":"254d304678038641f87aff42be68ef74","title":"开启分页"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"77e7ae5ca528b246f72e4983374da469","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"选项卡"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"dd22d35e3d8fc63fa51ff4e01668fab6","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"富文本编辑器"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"e22d3ebde93e719f9cbe9b9004c2eae8","parentId":"87c8451f951888e483fe7ec7668507fe","title":"低于桌面堆叠排列"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"1498b9a569c6e2e6562e79a834cc93f4","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"上传下载"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"326147b3715dd8698f4d84058404e83d","parentId":"254d304678038641f87aff42be68ef74","title":"自定义分页"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"3c02556f5054c9fdf6faf6574004e792","parentId":"623262eac50915905031d8d67d8702ea","title":"数据字典管理"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"3ea0bdfc67e946ddc62dd82d9858b540","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"进度条"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"f8c3125fc92513c4ef9153aca7d6e272","parentId":"87c8451f951888e483fe7ec7668507fe","title":"九宫格"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"10fd29fb35ea748ae75dcd84895997c0","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"导入导出"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"4553ac5aa6eb328b8f5e68506c528715","parentId":"254d304678038641f87aff42be68ef74","title":"开启头部工具栏"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"a03f6123a7a1be65e514edd28de0ae8d","parentId":"623262eac50915905031d8d67d8702ea","title":"个人设置"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"bad0d5cfbdf372cf8025d32053fa4be3","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"面板"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"67090df9bf7c076aee440df201436458","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"徽章"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"8601b3790fb4c239f5ea5a61b56cd208","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"系统消息"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"b2d074869324db44caae589ceabacdcb","parentId":"254d304678038641f87aff42be68ef74","title":"开启合计行"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"3c83e94d8c9499d3962e405a2dddeebb","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"时间线"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"60fda72e475b1536cb166b0af7741697","parentId":"91be39ac09cdc71c7435ffb94f611795","title":"验证码"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ee31b443c0eb5d142e29012f559d0ef6","parentId":"254d304678038641f87aff42be68ef74","title":"高度最大适应"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"9cee434d97d9f75d2f35b2c87bef99bf","parentId":"254d304678038641f87aff42be68ef74","title":"开启复选框"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"a68e3126a3a35f5a50141ca027c35ce8","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"动画"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"ee7e7bb6e0da465b0b686473acc9259f","parentId":"254d304678038641f87aff42be68ef74","title":"开启单选框"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"fddbacbd7477af853faac22ca34d3a55","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"辅助"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"524ab0ea6d1bd06ea8f7430acf258b41","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"通用弹层"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"7c6037cdf7687a75d823b18b76c2d03f","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"上传"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"a6645e6e95d7babef01e71effdd91d6d","parentId":"254d304678038641f87aff42be68ef74","title":"开启单元格编辑"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"de471ff81fd3b7b0708ea3fd1a493f78","parentId":"254d304678038641f87aff42be68ef74","title":"加入表单元素"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"e04db2a1c2d8d911b6961f0c29ffe02a","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"时间日期"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"50677a25a6a1cc3e3d71834ef84c2c39","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"静态表格"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"5a717dd0bdd94123900508d172088d48","parentId":"254d304678038641f87aff42be68ef74","title":"设置单元格样式"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"24ca613859f753247f86b55a13e4f7d1","parentId":"254d304678038641f87aff42be68ef74","title":"固定列"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"254d304678038641f87aff42be68ef74","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"数据表格"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"324c09d31cc6910fa5ae805a737d32a8","parentId":"254d304678038641f87aff42be68ef74","title":"数据操作"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"76cf30bf5db55a9204e38dec11079e2a","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"分页"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"6c6da7e850d5a779a1ca3f3763cbce0d","parentId":"254d304678038641f87aff42be68ef74","title":"解析任意数据格式"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"f9679bc12890ed472d0fec02dd3fad64","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"上传"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"38aba0fbe14e8c0f596ff40fb29532aa","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"颜色选择器"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"7a5be98a72343beb5bd4f73e2793acb3","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"滑块组件"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"8afeeaa1d6f5c139f8b8cecf6c451ef2","parentId":"254d304678038641f87aff42be68ef74","title":"监听行事件"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"7514b093df0e52d76814ce5a6fb5c868","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"评分"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"df3fb69cec5024ffdc1967ed729541b6","parentId":"254d304678038641f87aff42be68ef74","title":"数据表格的重载"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"afd48d274868f87f05fa4aad2d767bf9","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"轮播"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"f5872bede681da8b0a7d5a2c1a2b3146","parentId":"254d304678038641f87aff42be68ef74","title":"设置初始排序"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"22691e0c807693f3ad52dcb043a2e2f7","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"流加载"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"66f6ab8a5f40facb94ab9ff93cd14aaf","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"工具"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"9de651e122d377f59f08febfaa3a5418","parentId":"254d304678038641f87aff42be68ef74","title":"监听单元格事件"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"6abd38f3c7631bccffb49ade84dba77b","parentId":"2774f4f1c30a34a75dfcfe5f8945d370","title":"代码修饰"},{"checkArr":[{"isChecked":"0","type":"0"}],"id":"c8ac3f04aea6be178a23cb5f93ec71aa","parentId":"254d304678038641f87aff42be68ef74","title":"复杂表头"}],"msg":""}
void SysRoleController::setMenu(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{    
    QString roleId = request.getParameter("roleId");
    //查询所有菜单
    QList<SysMenu> menuList;
    QStringList condi;
    m_pHelper->getMenuList(menuList, condi, "order_no");


    QSet<int> menuIdSet;
    QList<SysRoleMenu> roleMenuList;
    condi.clear();
    if(!roleId.isEmpty())
        condi << QString("role_id=%1").arg(roleId);

    m_pHelper->getRoleMenuList(roleMenuList, condi, "");
    foreach (SysRoleMenu roleMenu, roleMenuList) {
        menuIdSet.insert(roleMenu.getMenuId());
    }

    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QJsonArray array;
    foreach (SysMenu menu, menuList) {

        QJsonArray checkArr;
        QJsonObject checkObj;
        checkObj.insert("type",  0);
        if(menuIdSet.contains(menu.getId()))
            checkObj.insert("isChecked", 1);
        else
            checkObj.insert("isChecked", 0);
        checkArr.append(checkObj);

        QJsonObject obj;
        obj.insert("id",        menu.getId());
        obj.insert("parentId",  menu.getParentId());
        obj.insert("title",     menu.getName());
        obj.insert("checkArr",  checkArr);

        array.append(obj);
    }

    ret.setData(array);
    ResponseUtil::replyJson(response, ret);
}

//获取角色名称
//http://localhost:9004/sys/role/roleName?id=?&access_token=?
//{"code":0,"data":{"createDate":1552316846000,"createName":"system","id":"ea291b3717f834d2add7b14e2a2464ca","memo":"","name":"管理员","orderNo":1,"roleNo":"001","versions":0},"msg":""}
void SysRoleController::getRoleName(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString idParam = request.getParameter("id");
    int id = idParam.toInt();
    if(id <= 0)
    {
        ResponseUtil::replyJson(response, ret);
        return;
    }

    SysRole role;
    role.setId(id);
    if(!m_pHelper->selectEntityById(&role))
    {
        ret.failedMsg(QStringLiteral("菜单不存在"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QJsonObject obj = role.toJson();
    ret.setData(obj);
    ResponseUtil::replyJson(response, ret);
}

//获取角色列表
//http://localhost:9004/sys/role/datagrid?field=orderNo&order=asc&access_token=?
//{"code":0,"count":5,"data":[{"createDate":1552552203000,"createName":"system","id":"338f5cdec10b84302344ef84e4570d0d","memo":"","name":"销售人员","orderNo":0,"parentId":"ea291b3717f834d2add7b14e2a2464ca","roleNo":"009","versions":0},{"createDate":1552552216000,"createName":"system","id":"3c67201c91bde61d709e4a2460f4e825","memo":"","name":"技术经理","orderNo":1,"parentId":"ea291b3717f834d2add7b14e2a2464ca","roleNo":"006","versions":0},{"createDate":1552359189000,"createName":"system","id":"4b860179c135ea23961cc1fb88926f97","memo":"","name":"技术人员","orderNo":1,"parentId":"3c67201c91bde61d709e4a2460f4e825","roleNo":"001","versions":1},{"createDate":1552316846000,"createName":"system","id":"ea291b3717f834d2add7b14e2a2464ca","memo":"","name":"管理员","orderNo":1,"parentId":"-1","roleNo":"001","versions":0},{"createDate":1552552192000,"createName":"system","id":"a088b47539612eebeace406a8f458910","memo":"2","name":"市场人员","orderNo":3,"parentId":"ea291b3717f834d2add7b14e2a2464ca","roleNo":"004","updateDate":1575272596000,"updateName":"admin","versions":3}],"msg":""}
void SysRoleController::datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{

    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QList<SysRole> roleList;
    QStringList condi;
    m_pHelper->getRoleList(roleList, condi, "order_no");

    QJsonArray array;
    foreach(SysRole role, roleList)
    {
        QJsonObject obj = role.toJson();
        if(role.getParentId() <=0 )
            obj["parentId"] = -1;
        array.append(obj);
    }

    ret.setProp("count", roleList.size());
    ret.setData(array);
    ResponseUtil::replyJson(response, ret);
}

//保存角色信息
//http://localhost:9004/sys/role/save
//
/*
id: ?
roleNo: 001
name: 技术人员
parentName: 技术经理
parentId: ?
orderNo: 1
memo:
access_token: ?
*/
void SysRoleController::save(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{

    SysRole role;

    QString parentIdParam = request.getParameter("parentId");
    if(parentIdParam.isEmpty())
        role.setParentId(-1);
    else
        role.setParentId(parentIdParam.toInt());

    QString orderNoParam = request.getParameter("orderNo");
    if(!orderNoParam.isEmpty())
        role.setOrderNo(orderNoParam.toInt());

    role.setRoleNo(request.getParameter("roleNo"));
    role.setName(request.getParameter("name"));
    role.setMemo(request.getParameter("memo"));

    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString idParam = request.getParameter("id");
    if(!idParam.isEmpty())
    {
        role.setId(idParam.toInt());
        role.setUpdateDate(QDateTime::currentDateTime());
        role.setUpdateName(visitUser.getUserName());

        if(!m_pHelper->updateEntity(&role))
        {
            ret.failedMsg(QStringLiteral("操作失败"));
        }

    }
    else
    {
        role.setCreateDate(QDateTime::currentDateTime());
        role.setCreateName(visitUser.getUserName());
        role.setUpdateDate(QDateTime::currentDateTime());
        role.setUpdateName(visitUser.getUserName());
        role.setVersion(1);

        if(!m_pHelper->insertEntity(&role))
        {
            ret.failedMsg(QStringLiteral("操作失败"));
        }
    }
    ResponseUtil::replyJson(response, ret);

    QJsonObject objParam = request.getParameterJson();
    objParam.insert("access_token", "?");
    SysLog log("system", SysLog::SAVE, QStringLiteral("保存角色信息"),
               visitUser.getUserName(),
                QString(__FILE__),
                QString(__FUNCTION__),
                GlobalFunc::JsonToString(objParam),
                request.getPeerAddress().toString(),
                request.getHeader("User-Agent"),
                QStringLiteral("保存角色信息成功"));
    m_pHelper->insertEntity(&log);
}

//删除角色信息
void SysRoleController::delRole(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString idParam = request.getParameter("id");
    if(idParam.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    int id = idParam.toInt();

    //该角色下存在用户，不能直接删除
    QStringList condi;
    condi << "role_id=" + id;
    QList<SysUserRole> userRoleList;
    m_pHelper->getUserRoleList(userRoleList, condi, "");
    if(userRoleList.size() > 0)
    {
        ret.failedMsg(QStringLiteral("该角色下存在用户，不能直接删除"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    //该角色下存在子角色，不能直接删除
    condi.clear();
    condi << "parent_id=" + id;
    QList<SysRole> roleList;
    m_pHelper->getRoleList(roleList, condi, "");
    if(roleList.size() > 0)
    {
        ret.failedMsg(QStringLiteral("角色下存在子角色，不能直接删除"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    SysRole role;
    role.setId(id);
    if(!m_pHelper->deleteEntity(&role))
    {
        ret.failedMsg(QStringLiteral("操作失败"));
    }

    ResponseUtil::replyJson(response, ret);

    QJsonObject objParam = request.getParameterJson();
    objParam.insert("access_token", "?");
    SysLog log("system", SysLog::DEL, QStringLiteral("删除角色信息"),
               visitUser.getUserName(),
                QString(__FILE__),
                QString(__FUNCTION__),
                GlobalFunc::JsonToString(objParam),
                request.getPeerAddress().toString(),
                request.getHeader("User-Agent"),
                QStringLiteral("删除角色信息成功"));
    m_pHelper->insertEntity(&log);
}

//保存权限菜单
//http://localhost:9004/sys/role/saverolemenu
//{"code":0,"success":true,"msg":"权限配置成功","data":null}
/*
roleId: ?
roleMenuIds: ?,?,?,?,?
access_token: ?
*/
void SysRoleController::saveRoleMenu(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    QString roleId = request.getParameter("roleId");
    QString roleMenuIds = request.getParameter("roleMenuIds");
    ResultJson ret(0, true, QStringLiteral("操作成功"));

    //删除之前的配置
    SysRoleMenu dao;
    QStringList condi;
    condi << QString("role_id=%1").arg(roleId);
    if(!m_pHelper->deleteTable(dao.getTableName(), condi))
    {
        ret.failedMsg(QStringLiteral("删除之前的配置失败"));
    }

    //保存新的配置
    QStringList menuIdList = roleMenuIds.split(",");
    for(int i=0; i < menuIdList.size(); i++)
    {
        QString menuId = menuIdList.at(i);

        SysRoleMenu rm;
        rm.setCreateDate(QDateTime::currentDateTime());
        rm.setCreateName(visitUser.getUserName());
        rm.setUpdateDate(QDateTime::currentDateTime());
        rm.setUpdateName(visitUser.getUserName());
        rm.setVersion(1);

        rm.setRoleId(roleId.toInt());
        rm.setMenuId(menuId.toInt());

        if(!m_pHelper->insertEntity(&rm))
        {
            ret.failedMsg(QStringLiteral("保存新的失败"));
            ResponseUtil::replyJson(response, ret);
            return;
        }
    }
    ResponseUtil::replyJson(response, ret);

    QJsonObject objParam = request.getParameterJson();
    objParam.insert("access_token", "?");
    SysLog log("system", SysLog::SAVE, QStringLiteral("保存权限菜单"),
               visitUser.getUserName(),
                QString(__FILE__),
                QString(__FUNCTION__),
                GlobalFunc::JsonToString(objParam),
                request.getPeerAddress().toString(),
                request.getHeader("User-Agent"),
                QStringLiteral("保存权限菜单成功"));
    m_pHelper->insertEntity(&log);
}
