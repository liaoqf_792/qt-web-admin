﻿#ifndef SYSUSERCONTROLLER_H
#define SYSUSERCONTROLLER_H

#include "global.h"
#include <QMap>

using namespace stefanfrings;

class SysUserController: public HttpRequestHandler {
    Q_OBJECT
    Q_DISABLE_COPY(SysUserController)

public:
    SysUserController();
    void service(HttpRequest& request, HttpResponse& response);
    void datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser);     //获取用户列表
    void roleByUserId(HttpRequest& request, HttpResponse& response, SysUser& visitUser); //获取角色信息
    void save(HttpRequest& request, HttpResponse& response, SysUser& visitUser);         //保存用户信息
    void deleteUser(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //删除用户信息
    void getName(HttpRequest& request, HttpResponse& response, SysUser& visitUser);      //获取用户姓名
    void info(HttpRequest& request, HttpResponse& response, SysUser& visitUser);         //保存基本资料
    void saveInfo(HttpRequest& request, HttpResponse& response, SysUser& visitUser);     //获取用户基本资料
    void savePwd(HttpRequest& request, HttpResponse& response, SysUser& visitUser);      //修改密码


private:
    SqlHelper* m_pHelper;
    typedef void (SysUserController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSUSERCONTROLLER_H
