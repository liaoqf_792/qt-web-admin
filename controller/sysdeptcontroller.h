﻿#ifndef SYSDEPTCONTROLLER_H
#define SYSDEPTCONTROLLER_H


#include "global.h"
#include <QMap>

class SysDeptController: public HttpRequestHandler
{
    Q_OBJECT
    Q_DISABLE_COPY(SysDeptController)

public:
    SysDeptController();
    void          service(HttpRequest& request, HttpResponse& response);
    void         deptType(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取部门类型
    void         deptName(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取部门名称
    void deptNameByUserId(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取用户部门
    void         datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //获取部门列表
    void             save(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //保存部门信息
    void          delDept(HttpRequest& request, HttpResponse& response, SysUser& visitUser);   //删除部门信息

private:
    SqlHelper* m_pHelper;
    typedef void (SysDeptController::*pServFunc)(HttpRequest& request, HttpResponse& response, SysUser& visitUser);
    QMap<QString, pServFunc> m_mapFunc;
};

#endif // SYSDEPTCONTROLLER_H
