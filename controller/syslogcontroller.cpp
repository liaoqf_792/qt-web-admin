﻿#include "syslogcontroller.h"
#include "domain/syslog.h"
#include "utils/dictionaryutil.h"

SysLogController::SysLogController()
{
    m_pHelper = DbMgr::instance()->getHelper();

    m_mapFunc.insert("/sys/log/datagrid",  &SysLogController::datagrid);  //获取系统日志列表
    m_mapFunc.insert("/sys/log/logtype",   &SysLogController::logType);   //获取日志类型
}

void SysLogController::service(HttpRequest& request, HttpResponse& response)
{
    qDebug("SysMenuController::service(HttpReques");
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString access_token = request.getParameter("access_token");
    QString username = JwtUtil::getUsername(access_token);
    if(username == NULL || username.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }
    SysUser visitUser;
    if(!m_pHelper->selectUserByName(visitUser, username))
    {
        ret.failedMsg(QStringLiteral("用户不存在，操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QByteArray path = request.getPath().toLower();
    if(m_mapFunc.contains(path))
    {
        pServFunc func = m_mapFunc.value(path);
        (this->*func)(request, response, visitUser);
    }
}

//获取日志类型
//http://localhost:9004/demo/sys/log/logtype
//{"code":0,"data":[{"label":"登录","value":"1"},{"label":"登出","value":"2"},{"label":"保存","value":"3"},{"label":"新增","value":"4"},{"label":"删除","value":"5"},{"label":"更新","value":"6"},{"label":"定时任务","value":"7"},{"label":"正常","value":"8"},{"label":"异常","value":"9"}],"msg":""}
void SysLogController::logType(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));

    QMap<QString, QString> map = DictionaryUtil::instance()->getDictionary("log_type");

    QMap<QString, QJsonObject> orderMap;
    foreach (QString key, map.keys()) {
         QJsonObject obj;
         obj.insert("label", key);
         obj.insert("value", map[key]);
         orderMap.insert(map[key], obj);
    }
    QJsonArray dataArray;
    foreach (QString key, orderMap.keys())
        dataArray.append(orderMap[key]);
    ret.setData(dataArray);
    ResponseUtil::replyJson(response, ret);
}

//获取系统日志列表
//http://localhost:9005/sys/log/datagrid
void SysLogController::datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    int page = 1;
    int limit = 20;
    QString field = "createDate";
    QString order = "desc";
    QString userName = "";
    QString type = "";

    if(request.getParameterMap().contains("page"))
    {
        page = request.getParameter("page").toInt();
    }
    if(request.getParameterMap().contains("limit"))
    {
        limit = request.getParameter("limit").toInt();
    }
    if(request.getParameterMap().contains("field"))
    {
        field = request.getParameter("field");
    }
    if(request.getParameterMap().contains("order"))
    {
        order = request.getParameter("order");
    }
    if(request.getParameterMap().contains("userName"))
    {
        userName = request.getParameter("userName");
    }
    if(request.getParameterMap().contains("type"))
    {
        type = request.getParameter("type");
    }

    QList<SysLog> logList;
    int count = 0;
    QStringList condi;
    if(!userName.isEmpty())
        condi << "user_name=" + userName;
    if(!type.isEmpty())
        condi << "type=" + type;
    m_pHelper->getLogList(logList, count, page, limit, condi, field, order=="asc");

    QMap<QString, QString> map = DictionaryUtil::instance()->getDictionary("log_type");

    QJsonArray dataArray;
    foreach(SysLog log, logList)
    {
        QJsonObject obj = log.toJson();

        foreach (QString key, map.keys()) {
            if(map[key] == log.getType())
            {
                obj.insert("type",  key);
                break;
            }
        }

        dataArray.append(obj);
    }

    ResultJson ret(0, true, QStringLiteral(""));
    ret.setProp("count", count);
    ret.setData(dataArray);
    ResponseUtil::replyJson(response, ret);
}
