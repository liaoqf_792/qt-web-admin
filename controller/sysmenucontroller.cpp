﻿#include "sysmenucontroller.h"
#include "utils/dictionaryutil.h"

SysMenuController::SysMenuController()
{
    m_pHelper = DbMgr::instance()->getHelper();

    m_mapFunc.insert("/sys/menu/datagrid",  &SysMenuController::datagrid);  //获取菜单列表
    m_mapFunc.insert("/sys/menu/list",      &SysMenuController::list);      //获取菜单类型
    m_mapFunc.insert("/sys/menu/menutype",  &SysMenuController::menuType);  //获取菜单类型
    m_mapFunc.insert("/sys/menu/menuname",  &SysMenuController::menuName);  //获取菜单名称
    m_mapFunc.insert("/sys/menu/save",      &SysMenuController::save);      //保存菜单
    m_mapFunc.insert("/sys/menu/delete",    &SysMenuController::delMenu);   //删除菜单信息
}

void SysMenuController::service(HttpRequest& request, HttpResponse& response)
{
    qDebug("SysMenuController::service(HttpReques");
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString access_token = request.getParameter("access_token");
    QString username = JwtUtil::getUsername(access_token);
    if(username == NULL || username.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }
    SysUser visitUser;
    if(!m_pHelper->selectUserByName(visitUser, username))
    {
        ret.failedMsg(QStringLiteral("用户不存在，操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QByteArray path = request.getPath().toLower();
    if(m_mapFunc.contains(path))
    {
        pServFunc func = m_mapFunc.value(path);
        (this->*func)(request, response, visitUser);
    }
}

// 获取权限菜单
// GET /sys/menu/list?access_token=
// {"code":0,"data":[],"msg":"操作成功","success":true}
void SysMenuController::list(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));

    //根据用户获取用户权限下的菜单
    //.....
    QStringList condi;
    QList<SysUserRole> userRoleList;
    QList<SysRoleMenu> roleMenuList;
    QList<SysMenu> menuList;
    m_pHelper->getMenuList(menuList, condi, "order_no");

    condi << QString("user_id=%1").arg(visitUser.getId());
    m_pHelper->getUserRoleList(userRoleList, condi, "id");
    if(userRoleList.size() == 0)
    {
        ret.okMsg(QStringLiteral("获取权限菜单为空"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    int roleId = userRoleList[0].getRoleId();
    condi.clear();
    condi << QString("role_id=%1").arg(roleId);
    m_pHelper->getRoleMenuList(roleMenuList, condi, "id");

    QList<SysMenu> userMenuList;
    foreach (SysMenu menu, menuList)
    {
        bool isFind = false;
        foreach (SysRoleMenu roleMenu, roleMenuList)
        {
            if(roleMenu.getMenuId() == menu.getId())
            {
                isFind = true;
                break;
            }
        }
        if(isFind)
            userMenuList.append(menu);
    }
    if(userMenuList.size() == 0)
    {
        ret.okMsg(QStringLiteral("获取权限菜单为空"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    SysMenu topMenu;
    topMenu.setId(0);
    QJsonArray menuArray = getMenuChildren(topMenu, userMenuList);
    ret.setData(menuArray);
    ResponseUtil::replyJson(response, ret);


//    //对于admin用户，获取所有菜单
//    if(visitUser.getUserName() == "admin")
//    {
//        QList<SysMenu> menuList;
//        QStringList tmp;
//        m_pHelper->getMenuList(menuList, tmp, "order_no");

//        SysMenu menu;
//        menu.setId(0);
//        QJsonArray menuArray = getMenuChildren(menu, menuList);
//        ret.setData(menuArray);
//        ResponseUtil::replyJson(response, ret);
//    }
//    else
//    {

//    }

//    QString list = QStringLiteral("[{\"icon\":\"layui-icon-home\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"index\",\"name\":\"\",\"spread\":false,\"title\":\"项目主页\"}],\"name\":\"\",\"spread\":false,\"title\":\"项目介绍\"},{\"icon\":\"layui-icon-component\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"sys/user/index\",\"name\":\"\",\"spread\":false,\"title\":\"用户管理\"},{\"icon\":\"\",\"jump\":\"sys/dept/index\",\"name\":\"\",\"spread\":false,\"title\":\"部门管理\"},{\"icon\":\"\",\"jump\":\"sys/role/index\",\"name\":\"\",\"spread\":false,\"title\":\"角色管理\"},{\"icon\":\"\",\"jump\":\"sys/menu/index\",\"name\":\"\",\"spread\":false,\"title\":\"菜单管理\"},{\"icon\":\"\",\"jump\":\"sys/log/index\",\"name\":\"\",\"spread\":false,\"title\":\"日志管理\"},{\"icon\":\"\",\"jump\":\"sys/dic/index\",\"name\":\"\",\"spread\":false,\"title\":\"数据字典管理\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"sys/set/info\",\"name\":\"\",\"spread\":false,\"title\":\"基本资料\"},{\"icon\":\"\",\"jump\":\"sys/set/password\",\"name\":\"\",\"spread\":false,\"title\":\"修改密码\"}],\"name\":\"\",\"spread\":false,\"title\":\"个人设置\"}],\"name\":\"\",\"spread\":false,\"title\":\"系统管理\"},{\"icon\":\"layui-icon-console\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"monitor/druid/sql\",\"name\":\"\",\"spread\":false,\"title\":\"druid 监控\"},{\"icon\":\"\",\"jump\":\"monitor/server/index\",\"name\":\"\",\"spread\":false,\"title\":\"服务器监控\"}],\"name\":\"\",\"spread\":false,\"title\":\"系统监控\"},{\"icon\":\"layui-icon-set\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"func/email/index\",\"name\":\"\",\"spread\":false,\"title\":\"邮件发送\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"func/sms/qcloudsms\",\"name\":\"\",\"spread\":false,\"title\":\"腾讯云短信\"},{\"icon\":\"\",\"jump\":\"func/sms/yunchaoyun\",\"name\":\"\",\"spread\":false,\"title\":\"云潮云短信\"},{\"icon\":\"\",\"jump\":\"func/sms/alisms\",\"name\":\"\",\"spread\":false,\"title\":\"阿里云短信\"}],\"name\":\"\",\"spread\":false,\"title\":\"短信服务\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"func/timer/job/index\",\"name\":\"\",\"spread\":false,\"title\":\"定时任务\"},{\"icon\":\"\",\"jump\":\"func/timer/joblog/index\",\"name\":\"\",\"spread\":false,\"title\":\"任务日志\"}],\"name\":\"\",\"spread\":false,\"title\":\"定时任务\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"func/layedit/layedit\",\"name\":\"\",\"spread\":false,\"title\":\"layedit 编辑器\"},{\"icon\":\"\",\"jump\":\"func/layedit/kzlayedit\",\"name\":\"\",\"spread\":false,\"title\":\"kz.layedit 编辑器\"},{\"icon\":\"\",\"jump\":\"func/layedit/tinymce\",\"name\":\"\",\"spread\":false,\"title\":\"tinymce 编辑器\"}],\"name\":\"\",\"spread\":false,\"title\":\"富文本编辑器\"},{\"icon\":\"\",\"jump\":\"func/upload/upload\",\"name\":\"\",\"spread\":false,\"title\":\"上传下载\"},{\"icon\":\"\",\"jump\":\"func/export/index\",\"name\":\"\",\"spread\":false,\"title\":\"导入导出\"},{\"icon\":\"\",\"jump\":\"func/message/sys/index\",\"name\":\"\",\"spread\":false,\"title\":\"系统消息\"},{\"icon\":\"\",\"jump\":\"func/captcha/captcha\",\"name\":\"\",\"spread\":false,\"title\":\"验证码\"}],\"name\":\"\",\"spread\":false,\"title\":\"常用功能\"},{\"icon\":\"layui-icon-layer\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"comp/grid/list\",\"name\":\"\",\"spread\":false,\"title\":\"等比例列表排列\"},{\"icon\":\"\",\"jump\":\"comp/grid/mobile\",\"name\":\"\",\"spread\":false,\"title\":\"按移动端排列\"},{\"icon\":\"\",\"jump\":\"comp/grid/mobile-pc\",\"name\":\"\",\"spread\":false,\"title\":\"移动桌面端组合\"},{\"icon\":\"\",\"jump\":\"comp/grid/all\",\"name\":\"\",\"spread\":false,\"title\":\"全端复杂组合\"},{\"icon\":\"\",\"jump\":\"comp/grid/stack\",\"name\":\"\",\"spread\":false,\"title\":\"低于桌面堆叠排列\"},{\"icon\":\"\",\"jump\":\"comp/grid/speed-dial\",\"name\":\"\",\"spread\":false,\"title\":\"九宫格\"}],\"name\":\"\",\"spread\":false,\"title\":\"栅格\"},{\"icon\":\"\",\"jump\":\"comp/button/index\",\"name\":\"\",\"spread\":false,\"title\":\"按钮\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"comp/form/element\",\"name\":\"\",\"spread\":false,\"title\":\"表单元素\"},{\"icon\":\"\",\"jump\":\"comp/form/group\",\"name\":\"\",\"spread\":false,\"title\":\"表单组合\"}],\"name\":\"\",\"spread\":false,\"title\":\"表单\"},{\"icon\":\"\",\"jump\":\"comp/nav/index\",\"name\":\"\",\"spread\":false,\"title\":\"导航\"},{\"icon\":\"\",\"jump\":\"comp/tabs/index\",\"name\":\"\",\"spread\":false,\"title\":\"选项卡\"},{\"icon\":\"\",\"jump\":\"comp/progress/index\",\"name\":\"\",\"spread\":false,\"title\":\"进度条\"},{\"icon\":\"\",\"jump\":\"comp/panel/index\",\"name\":\"\",\"spread\":false,\"title\":\"面板\"},{\"icon\":\"\",\"jump\":\"comp/badge/index\",\"name\":\"\",\"spread\":false,\"title\":\"徽章\"},{\"icon\":\"\",\"jump\":\"comp/timeline/index\",\"name\":\"\",\"spread\":false,\"title\":\"时间线\"},{\"icon\":\"\",\"jump\":\"comp/anim/index\",\"name\":\"\",\"spread\":false,\"title\":\"动画\"},{\"icon\":\"\",\"jump\":\"comp/auxiliar/index\",\"name\":\"\",\"spread\":false,\"title\":\"辅助\"},{\"icon\":\"\",\"jump\":\"\",\"name\":\"\",\"spread\":false,\"title\":\"上传\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"comp/layer/list\",\"name\":\"\",\"spread\":false,\"title\":\"功能演示\"},{\"icon\":\"\",\"jump\":\"comp/layer/special-demo\",\"name\":\"\",\"spread\":false,\"title\":\"特殊示例\"},{\"icon\":\"\",\"jump\":\"comp/layer/theme\",\"name\":\"\",\"spread\":false,\"title\":\"风格定制\"}],\"name\":\"\",\"spread\":false,\"title\":\"通用弹层\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"comp/laydate/demo1\",\"name\":\"\",\"spread\":false,\"title\":\"功能示例一\"},{\"icon\":\"\",\"jump\":\"comp/laydate/demo2\",\"name\":\"\",\"spread\":false,\"title\":\"功能示例二\"},{\"icon\":\"\",\"jump\":\"comp/laydate/theme\",\"name\":\"\",\"spread\":false,\"title\":\"设定主题\"},{\"icon\":\"\",\"jump\":\"comp/laydate/special-demo\",\"name\":\"\",\"spread\":false,\"title\":\"特殊示例\"}],\"name\":\"\",\"spread\":false,\"title\":\"时间日期\"},{\"icon\":\"\",\"jump\":\"comp/table/static\",\"name\":\"\",\"spread\":false,\"title\":\"静态表格\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"comp/table/simple\",\"name\":\"\",\"spread\":false,\"title\":\"简单数据表格\"},{\"icon\":\"\",\"jump\":\"comp/table/auto\",\"name\":\"\",\"spread\":false,\"title\":\"列宽自动分配\"},{\"icon\":\"\",\"jump\":\"comp/table/data\",\"name\":\"\",\"spread\":false,\"title\":\"赋值已知数据\"},{\"icon\":\"\",\"jump\":\"comp/table/tostatic\",\"name\":\"\",\"spread\":false,\"title\":\"转化静态表格\"},{\"icon\":\"\",\"jump\":\"comp/table/page\",\"name\":\"\",\"spread\":false,\"title\":\"开启分页\"},{\"icon\":\"\",\"jump\":\"comp/table/resetPage\",\"name\":\"\",\"spread\":false,\"title\":\"自定义分页\"},{\"icon\":\"\",\"jump\":\"comp/table/toolbar\",\"name\":\"\",\"spread\":false,\"title\":\"开启头部工具栏\"},{\"icon\":\"\",\"jump\":\"comp/table/totalRow\",\"name\":\"\",\"spread\":false,\"title\":\"开启合计行\"},{\"icon\":\"\",\"jump\":\"comp/table/height\",\"name\":\"\",\"spread\":false,\"title\":\"高度最大适应\"},{\"icon\":\"\",\"jump\":\"comp/table/checkbox\",\"name\":\"\",\"spread\":false,\"title\":\"开启复选框\"},{\"icon\":\"\",\"jump\":\"comp/table/radio\",\"name\":\"\",\"spread\":false,\"title\":\"开启单选框\"},{\"icon\":\"\",\"jump\":\"comp/table/cellEdit\",\"name\":\"\",\"spread\":false,\"title\":\"开启单元格编辑\"},{\"icon\":\"\",\"jump\":\"comp/table/form\",\"name\":\"\",\"spread\":false,\"title\":\"加入表单元素\"},{\"icon\":\"\",\"jump\":\"comp/table/style\",\"name\":\"\",\"spread\":false,\"title\":\"设置单元格样式\"},{\"icon\":\"\",\"jump\":\"comp/table/fixed\",\"name\":\"\",\"spread\":false,\"title\":\"固定列\"},{\"icon\":\"\",\"jump\":\"comp/table/operate\",\"name\":\"\",\"spread\":false,\"title\":\"数据操作\"},{\"icon\":\"\",\"jump\":\"comp/table/parseData\",\"name\":\"\",\"spread\":false,\"title\":\"解析任意数据格式\"},{\"icon\":\"\",\"jump\":\"comp/table/onrow\",\"name\":\"\",\"spread\":false,\"title\":\"监听行事件\"},{\"icon\":\"\",\"jump\":\"comp/table/reload\",\"name\":\"\",\"spread\":false,\"title\":\"数据表格的重载\"},{\"icon\":\"\",\"jump\":\"comp/table/initSort\",\"name\":\"\",\"spread\":false,\"title\":\"设置初始排序\"},{\"icon\":\"\",\"jump\":\"comp/table/cellEvent\",\"name\":\"\",\"spread\":false,\"title\":\"监听单元格事件\"},{\"icon\":\"\",\"jump\":\"comp/table/thead\",\"name\":\"\",\"spread\":false,\"title\":\"复杂表头\"}],\"name\":\"\",\"spread\":false,\"title\":\"数据表格\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"comp/laypage/demo1\",\"name\":\"\",\"spread\":false,\"title\":\"功能演示一\"},{\"icon\":\"\",\"jump\":\"comp/laypage/demo2\",\"name\":\"\",\"spread\":false,\"title\":\"功能演示二\"}],\"name\":\"\",\"spread\":false,\"title\":\"分页\"},{\"icon\":\"\",\"jump\":\"\",\"list\":[{\"icon\":\"\",\"jump\":\"comp/upload/demo1\",\"name\":\"\",\"spread\":false,\"title\":\"功能演示一\"},{\"icon\":\"\",\"jump\":\"comp/upload/demo2\",\"name\":\"\",\"spread\":false,\"title\":\"功能演示二\"}],\"name\":\"\",\"spread\":false,\"title\":\"上传\"},{\"icon\":\"\",\"jump\":\"comp/colorpicker/index\",\"name\":\"\",\"spread\":false,\"title\":\"颜色选择器\"},{\"icon\":\"\",\"jump\":\"comp/slider/index\",\"name\":\"\",\"spread\":false,\"title\":\"滑块组件\"},{\"icon\":\"\",\"jump\":\"comp/rate/index\",\"name\":\"\",\"spread\":false,\"title\":\"评分\"},{\"icon\":\"\",\"jump\":\"comp/carousel/index\",\"name\":\"\",\"spread\":false,\"title\":\"轮播\"},{\"icon\":\"\",\"jump\":\"comp/util/index\",\"name\":\"\",\"spread\":false,\"title\":\"工具\"},{\"icon\":\"\",\"jump\":\"comp/flow/index\",\"name\":\"\",\"spread\":false,\"title\":\"流加载\"},{\"icon\":\"\",\"jump\":\"comp/code/index\",\"name\":\"\",\"spread\":false,\"title\":\"代码修饰\"}],\"name\":\"\",\"spread\":false,\"title\":\"示例组件\"}]");
//    QJsonArray menuList = GlobalFunc::StringToArray(list);
//    ResultJson ret(0, true, QStringLiteral("操作成功"));
//    ret.setData(menuList);
//    ResponseUtil::replyJson(response, ret);
}

//获取菜单类型
//http://localhost:9004/sys/menu/menuType?access_token=?
//{"code":0,"data":[{"label":"菜单","value":"0"},{"label":"按钮","value":"1"}],"msg":""}
void SysMenuController::menuType(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{    
    ResultJson ret(0, true, QStringLiteral("操作成功"));

    QMap<QString, QString> map = DictionaryUtil::instance()->getDictionary("menu_type");

    QMap<QString, QJsonObject> orderMap;
    foreach (QString key, map.keys()) {
         QJsonObject obj;
         obj.insert("label", key);
         obj.insert("value", map[key]);
         orderMap.insert(map[key], obj);
    }
    QJsonArray dataArray;
    foreach (QString key, orderMap.keys())
        dataArray.append(orderMap[key]);
    ret.setData(dataArray);
    ResponseUtil::replyJson(response, ret);
}

//获取菜单类型
//http://localhost:9004/demo/sys/menu/menuName?id=36df3e985a7c01432cc579728425bf09&access_token=?
//{"code":0,"data":{"createDate":1575341975000,"createName":"admin","icon":"layui-icon-home","id":"?","memo":"1","name":"项目介绍","orderNo":0,"type":"0","updateDate":1575342007000,"updateName":"admin","url":"","versions":4},"msg":""}
void SysMenuController::menuName(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{

    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString idParam = request.getParameter("id");
    int id = idParam.toInt();
    if(id <= 0)
    {
        ResponseUtil::replyJson(response, ret);
        return;
    }

    SysMenu menu;
    menu.setId(id);
    if(!m_pHelper->selectEntityById(&menu))
    {
        ret.failedMsg(QStringLiteral("菜单不存在"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    QJsonObject obj = menu.toJson();
    ret.setData(obj);
    ResponseUtil::replyJson(response, ret);
}

//获取菜单列表
//http://localhost:9004/sys/menu/datagrid?field=orderNo&order=asc&access_token=?
//{"code":0,"count":120,"data":[{"createDate":1575341975000,"createName":"admin","icon":"layui-icon-home","id":"36df3e985a7c01432cc579728425bf09","memo":"1","name":"项目介绍","orderNo":0,"parentId":"-1","type":"菜单","updateDate":1575342007000,"updateName":"admin","url":"","versions":4},{"createDate":1575901922000,"createName":"admin","icon":"","id":"87c8451f951888e483fe7ec7668507fe","memo":"","name":"栅格","orderNo":0,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"","versions":0},{"createDate":1575943452000,"createName":"admin","icon":"","id":"0d079901d30c6bdbcae1349de1aabd30","memo":"","name":"功能示例一","orderNo":1,"parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","type":"菜单","url":"comp/laydate/demo1","versions":0},{"createDate":1575949415000,"createName":"admin","icon":"","id":"391f39b83b80cc3bd68e115d935f8109","memo":"","name":"功能演示一","orderNo":1,"parentId":"f9679bc12890ed472d0fec02dd3fad64","type":"菜单","url":"comp/upload/demo1","versions":0},{"createDate":1575949226000,"createName":"admin","icon":"","id":"3b362bf507f42d306368728df86877e0","memo":"","name":"功能演示一","orderNo":1,"parentId":"76cf30bf5db55a9204e38dec11079e2a","type":"菜单","url":"comp/laypage/demo1","versions":0},{"createDate":1578303165000,"createName":"admin","icon":"","id":"426c5207e1f95db3ca968ec508aea490","memo":"","name":"基本资料","orderNo":1,"parentId":"a03f6123a7a1be65e514edd28de0ae8d","type":"菜单","url":"sys/set/info","versions":0},{"createDate":1575612697000,"createName":"admin","icon":"","id":"4f8fa64d0ec7fce7c2cedc5ac6d09489","memo":"","name":"邮件发送","orderNo":1,"parentId":"91be39ac09cdc71c7435ffb94f611795","type":"菜单","updateDate":1575880308000,"updateName":"admin","url":"func/email/index","versions":2},{"createDate":1575944573000,"createName":"admin","icon":"","id":"59f249d45a9478beae170b4a4ce03b61","memo":"","name":"简单数据表格","orderNo":1,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/simple","versions":0},{"createDate":1552290668000,"createName":"system","icon":"layui-icon-component","id":"623262eac50915905031d8d67d8702ea","memo":"0","name":"系统管理","orderNo":1,"parentId":"-1","type":"菜单","updateDate":1554776320000,"updateName":"admin","url":"","versions":3},{"createDate":1575342131000,"createName":"admin","icon":"","id":"700bb6c4a8d5cd6b9d7016e4faae8628","memo":"","name":"项目主页","orderNo":1,"parentId":"36df3e985a7c01432cc579728425bf09","type":"菜单","updateDate":1577717177000,"updateName":"admin","url":"index","versions":1},{"createDate":1552923570000,"createName":"system","icon":"","id":"755956ba2ea812d8749a062f34b7da2d","memo":"","name":"用户管理","orderNo":1,"parentId":"623262eac50915905031d8d67d8702ea","type":"菜单","updateDate":1574931170000,"updateName":"admin","url":"sys/user/index","versions":2},{"createDate":1576822149000,"createName":"admin","icon":"","id":"75e8d5210cf7c50af4db8f560c020974","memo":"","name":"新增","orderNo":1,"parentId":"7de00ce887ea0c9c7144636112801a0c","type":"按钮","url":"sys/role/add","versions":0},{"createDate":1575901031000,"createName":"admin","icon":"","id":"84cc6a4dbda00fc1d3a24d55c9cd14e6","memo":"","name":"按钮","orderNo":1,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","updateDate":1575901428000,"updateName":"admin","url":"comp/button/index","versions":1},{"createDate":1575942916000,"createName":"admin","icon":"","id":"a7194554d65798806bac6732d36f67a4","memo":"","name":"功能演示","orderNo":1,"parentId":"524ab0ea6d1bd06ea8f7430acf258b41","type":"菜单","url":"comp/layer/list","versions":0},{"createDate":1576822045000,"createName":"admin","icon":"","id":"aa04121cc3d2e79286103acc24c35770","memo":"","name":"新增","orderNo":1,"parentId":"755956ba2ea812d8749a062f34b7da2d","type":"按钮","url":"sys/user/add","versions":0},{"createDate":1552294217000,"createName":"system","icon":"","id":"ae1dd4b597a544660759211fa1481b1b","memo":"","name":"新增","orderNo":1,"parentId":"6ee28a2031c8d1e701613082ea6ebc5e","type":"按钮","updateDate":1576822339000,"updateName":"admin","url":"sys/dept/add","versions":3},{"createDate":1575940749000,"createName":"admin","icon":"","id":"ae6e226f18c93b6fc93c1b6817242741","memo":"","name":"表单元素","orderNo":1,"parentId":"a9adea345ac1afeb3292e576bbfec804","type":"菜单","url":"comp/form/element","versions":0},{"createDate":1578026606000,"createName":"admin","icon":"","id":"be09904bbab6e8ad917bf6bf2d3c6ce7","memo":"","name":"定时任务","orderNo":1,"parentId":"213830032d2b72edf575f30d71361a51","type":"菜单","url":"func/timer/job/index","versions":0},{"createDate":1576822607000,"createName":"admin","icon":"","id":"c98b436c0bdf3e1db2aacbbaa6fcaca4","memo":"","name":"新增字典","orderNo":1,"parentId":"3c02556f5054c9fdf6faf6574004e792","type":"按钮","url":"sys/dic/add","versions":0},{"createDate":1576201530000,"createName":"admin","icon":"","id":"d4f0ee03c841e22d7cb1cbd271c63100","memo":"","name":"layedit 编辑器","orderNo":1,"parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","type":"菜单","updateDate":1576201613000,"updateName":"admin","url":"func/layedit/layedit","versions":2},{"createDate":1553847224000,"createName":"admin","icon":"","id":"da49ca0fe2ea8be4bb557c251c6180c3","memo":"","name":"druid 监控","orderNo":1,"parentId":"7476783545496331c211296efaa561a2","type":"菜单","updateDate":1553847234000,"updateName":"admin","url":"monitor/druid/sql","versions":5},{"createDate":1575901965000,"createName":"admin","icon":"","id":"fe70cf2fa1985a461186efcaf4633283","memo":"","name":"等比例列表排列","orderNo":1,"parentId":"87c8451f951888e483fe7ec7668507fe","type":"菜单","updateDate":1575901978000,"updateName":"admin","url":"comp/grid/list","versions":2},{"createDate":1576822436000,"createName":"admin","icon":"","id":"ff358bb4ca406915664f8cb8b848e9da","memo":"","name":"新增","orderNo":1,"parentId":"ecacd9efd6c237b80e46df8b1507a904","type":"按钮","url":"sys/menu/add","versions":0},{"createDate":1576821969000,"createName":"admin","icon":"","id":"19587742caad08e1c5eaa9483b02f49b","memo":"","name":"保存","orderNo":2,"parentId":"755956ba2ea812d8749a062f34b7da2d","type":"按钮","updateDate":1576821980000,"updateName":"admin","url":"sys:user:save","versions":1},{"createDate":1575967748000,"createName":"admin","icon":"","id":"1fc380fb7a2b3992e5f0dce39a683ce3","memo":"","name":"腾讯云短信","orderNo":2,"parentId":"28d1d2a835dc364315cc7f9832d664a0","type":"菜单","updateDate":1575967830000,"updateName":"admin","url":"func/sms/qcloudsms","versions":1},{"createDate":1575360883000,"createName":"admin","icon":"","id":"4b2e9da372c5c7b5d4d39c0f9a4e402d","memo":"","name":"服务器监控","orderNo":2,"parentId":"7476783545496331c211296efaa561a2","type":"菜单","url":"monitor/server/index","versions":0},{"createDate":1552291284000,"createName":"system","icon":"","id":"6ee28a2031c8d1e701613082ea6ebc5e","memo":"","name":"部门管理","orderNo":2,"parentId":"623262eac50915905031d8d67d8702ea","type":"菜单","updateDate":1574931177000,"updateName":"admin","url":"sys/dept/index","versions":5},{"createDate":1575949438000,"createName":"admin","icon":"","id":"73c52763764317a58bb2b7db884e8788","memo":"","name":"功能演示二","orderNo":2,"parentId":"f9679bc12890ed472d0fec02dd3fad64","type":"菜单","url":"comp/upload/demo2","versions":0},{"createDate":1575342166000,"createName":"admin","icon":"layui-icon-console","id":"7476783545496331c211296efaa561a2","memo":"","name":"系统监控","orderNo":2,"parentId":"-1","type":"菜单","updateDate":1575342198000,"updateName":"admin","url":"","versions":1},{"createDate":1576038535000,"createName":"admin","icon":"","id":"7e106236ac4833f27ffa0250d3492071","memo":"","name":"示例主页一","orderNo":2,"parentId":"36df3e985a7c01432cc579728425bf09","type":"菜单","updateDate":1576038554000,"updateName":"admin","url":"demo/main/homepage1","versions":1},{"createDate":1576822646000,"createName":"admin","icon":"","id":"83b3db7973133104ba24135b453939ce","memo":"","name":"新增字典值","orderNo":2,"parentId":"3c02556f5054c9fdf6faf6574004e792","type":"按钮","url":"sys/dic/addval","versions":0},{"createDate":1575949256000,"createName":"admin","icon":"","id":"9e278b9a4ee34914d1dd1969e800eb67","memo":"","name":"功能演示二","orderNo":2,"parentId":"76cf30bf5db55a9204e38dec11079e2a","type":"菜单","url":"comp/laypage/demo2","versions":0},{"createDate":1575943478000,"createName":"admin","icon":"","id":"ac698e2709fdcb90ee8b9ed88e5905d1","memo":"","name":"功能示例二","orderNo":2,"parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","type":"菜单","updateDate":1575943636000,"updateName":"admin","url":"comp/laydate/demo2","versions":1},{"createDate":1576227397000,"createName":"admin","icon":"","id":"adc1f1a492ac837bfec6292265c3b30d","memo":"","name":"kz.layedit 编辑器","orderNo":2,"parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","type":"菜单","url":"func/layedit/kzlayedit","versions":0},{"createDate":1576822464000,"createName":"admin","icon":"","id":"af52b09768353133a4d3aab4f167d4c4","memo":"","name":"保存","orderNo":2,"parentId":"ecacd9efd6c237b80e46df8b1507a904","type":"按钮","url":"sys:menu:save","versions":0},{"createDate":1578302669000,"createName":"admin","icon":"","id":"b44ad72e6203953c8f6a74521d05c8aa","memo":"","name":"修改密码","orderNo":2,"parentId":"a03f6123a7a1be65e514edd28de0ae8d","type":"菜单","url":"sys/set/password","versions":0},{"createDate":1575942974000,"createName":"admin","icon":"","id":"b7eadf3fdfe67c7ff3223c838213a584","memo":"","name":"特殊示例","orderNo":2,"parentId":"524ab0ea6d1bd06ea8f7430acf258b41","type":"菜单","url":"comp/layer/special-demo","versions":0},{"createDate":1575940792000,"createName":"admin","icon":"","id":"be00c02c463f91af6433918bee8219c4","memo":"","name":"表单组合","orderNo":2,"parentId":"a9adea345ac1afeb3292e576bbfec804","type":"菜单","url":"comp/form/group","versions":0},{"createDate":1576822195000,"createName":"admin","icon":"","id":"c9f52990bc82165f63e45df34077896c","memo":"","name":"保存角色","orderNo":2,"parentId":"7de00ce887ea0c9c7144636112801a0c","type":"按钮","updateDate":1576822753000,"updateName":"admin","url":"sys:role:save","versions":1},{"createDate":1575269231000,"createName":"admin","icon":"","id":"d080eec50903b82466fe1aa19f78b01d","memo":"","name":"保存","orderNo":2,"parentId":"6ee28a2031c8d1e701613082ea6ebc5e","type":"按钮","updateDate":1575269605000,"updateName":"admin","url":"sys:dept:save","versions":1},{"createDate":1578026631000,"createName":"admin","icon":"","id":"ecf0f6a8f0a34d0587695c5351c073d3","memo":"","name":"任务日志","orderNo":2,"parentId":"213830032d2b72edf575f30d71361a51","type":"菜单","url":"func/timer/joblog/index","versions":0},{"createDate":1575944661000,"createName":"admin","icon":"","id":"ee2ca956201793d7f035d2ae6e944c93","memo":"","name":"列宽自动分配","orderNo":2,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/auto","versions":0},{"createDate":1575902016000,"createName":"admin","icon":"","id":"f4bc6809cec463e0206deb80319dfb97","memo":"","name":"按移动端排列","orderNo":2,"parentId":"87c8451f951888e483fe7ec7668507fe","type":"菜单","updateDate":1575902069000,"updateName":"admin","url":"comp/grid/mobile","versions":1},{"createDate":1576465429000,"createName":"admin","icon":"","id":"0537fa19001c1758fe1d7e8ae93cb61a","memo":"","name":"阿里云短信","orderNo":3,"parentId":"28d1d2a835dc364315cc7f9832d664a0","type":"菜单","url":"func/sms/alisms","versions":0},{"createDate":1575943018000,"createName":"admin","icon":"","id":"06428eaef123e23852946540710eafe7","memo":"","name":"风格定制","orderNo":3,"parentId":"524ab0ea6d1bd06ea8f7430acf258b41","type":"菜单","url":"comp/layer/theme","versions":0},{"createDate":1576822693000,"createName":"admin","icon":"","id":"0f5a97c074f42292c477c1ecb8e58769","memo":"","name":"保存字典","orderNo":3,"parentId":"3c02556f5054c9fdf6faf6574004e792","type":"按钮","url":"sys:dic:save","versions":0},{"createDate":1575944712000,"createName":"admin","icon":"","id":"1b52228fe7c8b9cf259f9f7c9bfcd844","memo":"","name":"赋值已知数据","orderNo":3,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/data","versions":0},{"createDate":1576038581000,"createName":"admin","icon":"","id":"207ae629bfbf53290f3c64f1933d5ae6","memo":"","name":"示例主页二","orderNo":3,"parentId":"36df3e985a7c01432cc579728425bf09","type":"菜单","url":"demo/main/homepage2","versions":0},{"createDate":1575959479000,"createName":"admin","icon":"","id":"28d1d2a835dc364315cc7f9832d664a0","memo":"","name":"短信服务","orderNo":3,"parentId":"91be39ac09cdc71c7435ffb94f611795","type":"菜单","url":"","versions":0},{"createDate":1575959501000,"createName":"admin","icon":"","id":"3d0d3eed4985181a82a6387a30974a9c","memo":"","name":"云潮云短信","orderNo":3,"parentId":"28d1d2a835dc364315cc7f9832d664a0","type":"菜单","url":"func/sms/yunchaoyun","versions":0},{"createDate":1576822330000,"createName":"admin","icon":"","id":"427d532a84547250e510a3dcc388c92f","memo":"","name":"删除","orderNo":3,"parentId":"6ee28a2031c8d1e701613082ea6ebc5e","type":"按钮","url":"sys:dept:delete","versions":0},{"createDate":1576215596000,"createName":"admin","icon":"","id":"4388d13e00a1a94300c5fa7ff32d9d90","memo":"","name":"tinymce 编辑器","orderNo":3,"parentId":"dd22d35e3d8fc63fa51ff4e01668fab6","type":"菜单","updateDate":1576217595000,"updateName":"admin","url":"func/layedit/tinymce","versions":2},{"createDate":1576822372000,"createName":"admin","icon":"","id":"6aa4271aff8644eeb1561f57f8320446","memo":"","name":"删除","orderNo":3,"parentId":"755956ba2ea812d8749a062f34b7da2d","type":"按钮","url":"sys:user:delete","versions":0},{"createDate":1576822492000,"createName":"admin","icon":"","id":"7bf0c837223ed4b2693eadc933d5de56","memo":"","name":"删除","orderNo":3,"parentId":"ecacd9efd6c237b80e46df8b1507a904","type":"按钮","url":"sys:menu:delete","versions":0},{"createDate":1552923541000,"createName":"system","icon":"","id":"7de00ce887ea0c9c7144636112801a0c","memo":"","name":"角色管理","orderNo":3,"parentId":"623262eac50915905031d8d67d8702ea","type":"菜单","url":"sys/role/index","versions":0},{"createDate":1576822255000,"createName":"admin","icon":"","id":"7e3a22591b016b3ce6ccc881e7b9d294","memo":"","name":"保存权限","orderNo":3,"parentId":"7de00ce887ea0c9c7144636112801a0c","type":"按钮","url":"sys:role:saverolemenu","versions":0},{"createDate":1575902103000,"createName":"admin","icon":"","id":"8eec007fbd23c0a318ba3e74f9c3d2a9","memo":"","name":"移动桌面端组合","orderNo":3,"parentId":"87c8451f951888e483fe7ec7668507fe","type":"菜单","updateDate":1575902124000,"updateName":"admin","url":"comp/grid/mobile-pc","versions":1},{"createDate":1575612023000,"createName":"admin","icon":"layui-icon-set","id":"91be39ac09cdc71c7435ffb94f611795","memo":"","name":"常用功能","orderNo":3,"parentId":"-1","type":"菜单","updateDate":1575612038000,"updateName":"admin","url":"","versions":3},{"createDate":1575940598000,"createName":"admin","icon":"","id":"a9adea345ac1afeb3292e576bbfec804","memo":"","name":"表单","orderNo":3,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"","versions":0},{"createDate":1575943625000,"createName":"admin","icon":"","id":"ab83dd17afdebc0ffe0d20318e23ea07","memo":"","name":"设定主题","orderNo":3,"parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","type":"菜单","url":"comp/laydate/theme","versions":0},{"createDate":1576031583000,"createName":"admin","icon":"","id":"213830032d2b72edf575f30d71361a51","memo":"","name":"定时任务","orderNo":4,"parentId":"91be39ac09cdc71c7435ffb94f611795","type":"菜单","updateDate":1576031696000,"updateName":"admin","url":"","versions":3},{"createDate":1576038610000,"createName":"admin","icon":"","id":"33ad4150fead0f227d7c6adb3d4fde2f","memo":"","name":"示例主页三","orderNo":4,"parentId":"36df3e985a7c01432cc579728425bf09","type":"菜单","url":"demo/main/homepage3","versions":0},{"createDate":1575902202000,"createName":"admin","icon":"","id":"40cad6aa986f987303647287352f7788","memo":"","name":"全端复杂组合","orderNo":4,"parentId":"87c8451f951888e483fe7ec7668507fe","type":"菜单","url":"comp/grid/all","versions":0},{"createDate":1575944793000,"createName":"admin","icon":"","id":"4618aa278da605c57895074aee4e095d","memo":"","name":"转化静态表格","orderNo":4,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/tostatic","versions":0},{"createDate":1575943670000,"createName":"admin","icon":"","id":"612265d33877f681c26b2180f90a3e62","memo":"","name":"特殊示例","orderNo":4,"parentId":"e04db2a1c2d8d911b6961f0c29ffe02a","type":"菜单","url":"comp/laydate/special-demo","versions":0},{"createDate":1576822727000,"createName":"admin","icon":"","id":"a5efa8ea44340e69710c398838c78989","memo":"","name":"保存字典值","orderNo":4,"parentId":"3c02556f5054c9fdf6faf6574004e792","type":"按钮","url":"sys:dic:saveval","versions":0},{"createDate":1576822290000,"createName":"admin","icon":"","id":"bf5b7351e458a15a1ad7cf4421eb9493","memo":"","name":"删除","orderNo":4,"parentId":"7de00ce887ea0c9c7144636112801a0c","type":"按钮","updateDate":1576823130000,"updateName":"admin","url":"sys:role:delete","versions":1},{"createDate":1552923516000,"createName":"system","icon":"","id":"ecacd9efd6c237b80e46df8b1507a904","memo":"","name":"菜单管理","orderNo":4,"parentId":"623262eac50915905031d8d67d8702ea","type":"菜单","updateDate":1574931201000,"updateName":"admin","url":"sys/menu/index","versions":3},{"createDate":1575941083000,"createName":"admin","icon":"","id":"ff544ef46b64157f10b8c20a3ae33d0d","memo":"","name":"导航","orderNo":4,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/nav/index","versions":0},{"createDate":1575900985000,"createName":"admin","icon":"layui-icon-layer","id":"2774f4f1c30a34a75dfcfe5f8945d370","memo":"","name":"示例组件","orderNo":5,"parentId":"-1","type":"菜单","updateDate":1575901005000,"updateName":"admin","url":"","versions":2},{"createDate":1554087462000,"createName":"admin","icon":"","id":"2f9b5ee346d667a5c146b3a25009bc8c","memo":"0","name":"日志管理","orderNo":5,"parentId":"623262eac50915905031d8d67d8702ea","type":"菜单","updateDate":1554087485000,"updateName":"admin","url":"sys/log/index","versions":5},{"createDate":1576822792000,"createName":"admin","icon":"","id":"3e5df1746ba7a3be358e41cedff4c825","memo":"","name":"删除","orderNo":5,"parentId":"3c02556f5054c9fdf6faf6574004e792","type":"按钮","url":"sys:dic:delete","versions":0},{"createDate":1575944837000,"createName":"admin","icon":"","id":"54c7509f7a0fca385c935e4919dc10e7","memo":"","name":"开启分页","orderNo":5,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/page","versions":0},{"createDate":1575941181000,"createName":"admin","icon":"","id":"77e7ae5ca528b246f72e4983374da469","memo":"","name":"选项卡","orderNo":5,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/tabs/index","versions":0},{"createDate":1576201256000,"createName":"admin","icon":"","id":"dd22d35e3d8fc63fa51ff4e01668fab6","memo":"","name":"富文本编辑器","orderNo":5,"parentId":"91be39ac09cdc71c7435ffb94f611795","type":"菜单","url":"","versions":0},{"createDate":1575902249000,"createName":"admin","icon":"","id":"e22d3ebde93e719f9cbe9b9004c2eae8","memo":"","name":"低于桌面堆叠排列","orderNo":5,"parentId":"87c8451f951888e483fe7ec7668507fe","type":"菜单","updateDate":1575902316000,"updateName":"admin","url":"comp/grid/stack","versions":1},{"createDate":1576461588000,"createName":"admin","icon":"","id":"1498b9a569c6e2e6562e79a834cc93f4","memo":"","name":"上传下载","orderNo":6,"parentId":"91be39ac09cdc71c7435ffb94f611795","type":"菜单","updateDate":1576480324000,"updateName":"admin","url":"func/upload/upload","versions":2},{"createDate":1575944870000,"createName":"admin","icon":"","id":"326147b3715dd8698f4d84058404e83d","memo":"","name":"自定义分页","orderNo":6,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/resetPage","versions":0},{"createDate":1552291347000,"createName":"system","icon":"","id":"3c02556f5054c9fdf6faf6574004e792","memo":"","name":"数据字典管理","orderNo":6,"parentId":"623262eac50915905031d8d67d8702ea","type":"菜单","updateDate":1574931187000,"updateName":"admin","url":"sys/dic/index","versions":4},{"createDate":1575941498000,"createName":"admin","icon":"","id":"3ea0bdfc67e946ddc62dd82d9858b540","memo":"","name":"进度条","orderNo":6,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/progress/index","versions":0},{"createDate":1575902302000,"createName":"admin","icon":"","id":"f8c3125fc92513c4ef9153aca7d6e272","memo":"","name":"九宫格","orderNo":6,"parentId":"87c8451f951888e483fe7ec7668507fe","type":"菜单","updateDate":1575902394000,"updateName":"admin","url":"comp/grid/speed-dial","versions":1},{"createDate":1576551267000,"createName":"admin","icon":"","id":"10fd29fb35ea748ae75dcd84895997c0","memo":"","name":"导入导出","orderNo":7,"parentId":"91be39ac09cdc71c7435ffb94f611795","type":"菜单","updateDate":1576551287000,"updateName":"admin","url":"func/export/index","versions":2},{"createDate":1575944955000,"createName":"admin","icon":"","id":"4553ac5aa6eb328b8f5e68506c528715","memo":"","name":"开启头部工具栏","orderNo":7,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/toolbar","versions":0},{"createDate":1578302636000,"createName":"admin","icon":"","id":"a03f6123a7a1be65e514edd28de0ae8d","memo":"","name":"个人设置","orderNo":7,"parentId":"623262eac50915905031d8d67d8702ea","type":"菜单","url":"","versions":0},{"createDate":1575941554000,"createName":"admin","icon":"","id":"bad0d5cfbdf372cf8025d32053fa4be3","memo":"","name":"面板","orderNo":7,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/panel/index","versions":0},{"createDate":1575942079000,"createName":"admin","icon":"","id":"67090df9bf7c076aee440df201436458","memo":"","name":"徽章","orderNo":8,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/badge/index","versions":0},{"createDate":1576657752000,"createName":"admin","icon":"","id":"8601b3790fb4c239f5ea5a61b56cd208","memo":"","name":"系统消息","orderNo":8,"parentId":"91be39ac09cdc71c7435ffb94f611795","type":"菜单","updateDate":1576725638000,"updateName":"admin","url":"func/message/sys/index","versions":4},{"createDate":1575944996000,"createName":"admin","icon":"","id":"b2d074869324db44caae589ceabacdcb","memo":"","name":"开启合计行","orderNo":8,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/totalRow","versions":0},{"createDate":1575942377000,"createName":"admin","icon":"","id":"3c83e94d8c9499d3962e405a2dddeebb","memo":"","name":"时间线","orderNo":9,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/timeline/index","versions":0},{"createDate":1576742416000,"createName":"admin","icon":"","id":"60fda72e475b1536cb166b0af7741697","memo":"","name":"验证码","orderNo":9,"parentId":"91be39ac09cdc71c7435ffb94f611795","type":"菜单","url":"func/captcha/captcha","versions":0},{"createDate":1575945064000,"createName":"admin","icon":"","id":"ee31b443c0eb5d142e29012f559d0ef6","memo":"","name":"高度最大适应","orderNo":9,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/height","versions":0},{"createDate":1575945108000,"createName":"admin","icon":"","id":"9cee434d97d9f75d2f35b2c87bef99bf","memo":"","name":"开启复选框","orderNo":10,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/checkbox","versions":0},{"createDate":1575942481000,"createName":"admin","icon":"","id":"a68e3126a3a35f5a50141ca027c35ce8","memo":"","name":"动画","orderNo":10,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/anim/index","versions":0},{"createDate":1575945149000,"createName":"admin","icon":"","id":"ee7e7bb6e0da465b0b686473acc9259f","memo":"","name":"开启单选框","orderNo":11,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/radio","versions":0},{"createDate":1575942704000,"createName":"admin","icon":"","id":"fddbacbd7477af853faac22ca34d3a55","memo":"","name":"辅助","orderNo":11,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/auxiliar/index","versions":0},{"createDate":1575942751000,"createName":"admin","icon":"","id":"524ab0ea6d1bd06ea8f7430acf258b41","memo":"","name":"通用弹层","orderNo":12,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"","versions":0},{"createDate":1575949393000,"createName":"admin","icon":"","id":"7c6037cdf7687a75d823b18b76c2d03f","memo":"","name":"上传","orderNo":12,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"","versions":0},{"createDate":1575946981000,"createName":"admin","icon":"","id":"a6645e6e95d7babef01e71effdd91d6d","memo":"","name":"开启单元格编辑","orderNo":12,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","updateDate":1575946988000,"updateName":"admin","url":"comp/table/cellEdit","versions":1},{"createDate":1575947026000,"createName":"admin","icon":"","id":"de471ff81fd3b7b0708ea3fd1a493f78","memo":"","name":"加入表单元素","orderNo":13,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/form","versions":0},{"createDate":1575943066000,"createName":"admin","icon":"","id":"e04db2a1c2d8d911b6961f0c29ffe02a","memo":"","name":"时间日期","orderNo":13,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"","versions":0},{"createDate":1575943771000,"createName":"admin","icon":"","id":"50677a25a6a1cc3e3d71834ef84c2c39","memo":"","name":"静态表格","orderNo":14,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/table/static","versions":0},{"createDate":1575947067000,"createName":"admin","icon":"","id":"5a717dd0bdd94123900508d172088d48","memo":"","name":"设置单元格样式","orderNo":14,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/style","versions":0},{"createDate":1575947104000,"createName":"admin","icon":"","id":"24ca613859f753247f86b55a13e4f7d1","memo":"","name":"固定列","orderNo":15,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/fixed","versions":0},{"createDate":1575944532000,"createName":"admin","icon":"","id":"254d304678038641f87aff42be68ef74","memo":"","name":"数据表格","orderNo":15,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"","versions":0},{"createDate":1575947145000,"createName":"admin","icon":"","id":"324c09d31cc6910fa5ae805a737d32a8","memo":"","name":"数据操作","orderNo":16,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/operate","versions":0},{"createDate":1575949006000,"createName":"admin","icon":"","id":"76cf30bf5db55a9204e38dec11079e2a","memo":"","name":"分页","orderNo":16,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","updateDate":1575949013000,"updateName":"admin","url":"","versions":1},{"createDate":1575947182000,"createName":"admin","icon":"","id":"6c6da7e850d5a779a1ca3f3763cbce0d","memo":"","name":"解析任意数据格式","orderNo":17,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/parseData","versions":0},{"createDate":1575949286000,"createName":"admin","icon":"","id":"f9679bc12890ed472d0fec02dd3fad64","memo":"","name":"上传","orderNo":17,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","updateDate":1575949293000,"updateName":"admin","url":"","versions":1},{"createDate":1575951712000,"createName":"admin","icon":"","id":"38aba0fbe14e8c0f596ff40fb29532aa","memo":"","name":"颜色选择器","orderNo":18,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/colorpicker/index","versions":0},{"createDate":1575951805000,"createName":"admin","icon":"","id":"7a5be98a72343beb5bd4f73e2793acb3","memo":"","name":"滑块组件","orderNo":19,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/slider/index","versions":0},{"createDate":1575947224000,"createName":"admin","icon":"","id":"8afeeaa1d6f5c139f8b8cecf6c451ef2","memo":"","name":"监听行事件","orderNo":19,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/onrow","versions":0},{"createDate":1575951896000,"createName":"admin","icon":"","id":"7514b093df0e52d76814ce5a6fb5c868","memo":"","name":"评分","orderNo":20,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/rate/index","versions":0},{"createDate":1575947266000,"createName":"admin","icon":"","id":"df3fb69cec5024ffdc1967ed729541b6","memo":"","name":"数据表格的重载","orderNo":20,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/reload","versions":0},{"createDate":1575951933000,"createName":"admin","icon":"","id":"afd48d274868f87f05fa4aad2d767bf9","memo":"","name":"轮播","orderNo":21,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/carousel/index","versions":0},{"createDate":1575947298000,"createName":"admin","icon":"","id":"f5872bede681da8b0a7d5a2c1a2b3146","memo":"","name":"设置初始排序","orderNo":21,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/initSort","versions":0},{"createDate":1575952006000,"createName":"admin","icon":"","id":"22691e0c807693f3ad52dcb043a2e2f7","memo":"","name":"流加载","orderNo":22,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/flow/index","versions":0},{"createDate":1575952090000,"createName":"admin","icon":"","id":"66f6ab8a5f40facb94ab9ff93cd14aaf","memo":"","name":"工具","orderNo":22,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/util/index","versions":0},{"createDate":1575947335000,"createName":"admin","icon":"","id":"9de651e122d377f59f08febfaa3a5418","memo":"","name":"监听单元格事件","orderNo":22,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","updateDate":1575947354000,"updateName":"admin","url":"comp/table/cellEvent","versions":1},{"createDate":1575952211000,"createName":"admin","icon":"","id":"6abd38f3c7631bccffb49ade84dba77b","memo":"","name":"代码修饰","orderNo":23,"parentId":"2774f4f1c30a34a75dfcfe5f8945d370","type":"菜单","url":"comp/code/index","versions":0},{"createDate":1575947384000,"createName":"admin","icon":"","id":"c8ac3f04aea6be178a23cb5f93ec71aa","memo":"","name":"复杂表头","orderNo":23,"parentId":"254d304678038641f87aff42be68ef74","type":"菜单","url":"comp/table/thead","versions":0}],"msg":""}
void SysMenuController::datagrid(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QList<SysMenu> menuList;
    QStringList tmp;
    m_pHelper->getMenuList(menuList, tmp, "");

    QMap<QString, QString> map = DictionaryUtil::instance()->getDictionary("menu_type");
    QJsonArray array;
    foreach(SysMenu menu, menuList)
    {
        QJsonObject obj = menu.toJson();

        foreach(QString key, map.keys())
        {
            if(map[key] == menu.getType())
                obj["type"] = key;
        }
        if(menu.getParentId() <=0 )
            obj["parentId"] = -1;
        array.append(obj);
    }

    ret.setProp("count", menuList.size());
    ret.setData(array);
    ResponseUtil::replyJson(response, ret);
}

//保存菜单信息
//http://localhost:9004/sys/menu/save
//{"code":0,"success":true,"msg":"操作成功","data":null}
/*
id:
name: ddd
parentName: %E4%B8%8A%E4%BC%A0
parentId: ?
type: 0
url: fff
icon: rrr
orderNo: 2
memo:
access_token:
*/
void SysMenuController::save(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    SysMenu menu;
    QString parentIdParam = request.getParameter("parentId");
    if(parentIdParam.isEmpty())
        menu.setParentId(-1);
    else
        menu.setParentId(parentIdParam.toInt());

    QString orderNoParam = request.getParameter("orderNo");
    if(!orderNoParam.isEmpty())
        menu.setOrderNo(orderNoParam.toInt());

    menu.setName(request.getParameter("name"));
    menu.setType(request.getParameter("type"));
    menu.setUrl(request.getParameter("url"));
    menu.setIcon(request.getParameter("icon"));
    menu.setMemo(request.getParameter("memo"));


    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString idParam = request.getParameter("id");
    if(!idParam.isEmpty())
    {
        menu.setId(idParam.toInt());
        menu.setUpdateDate(QDateTime::currentDateTime());
        menu.setUpdateName(visitUser.getUserName());

        if(!m_pHelper->updateEntity(&menu))
        {
            ret.failedMsg(QStringLiteral("操作失败"));
        }

    }
    else
    {
        menu.setCreateDate(QDateTime::currentDateTime());
        menu.setCreateName(visitUser.getUserName());
        menu.setUpdateDate(QDateTime::currentDateTime());
        menu.setUpdateName(visitUser.getUserName());
        menu.setVersion(1);

        if(!m_pHelper->insertEntity(&menu))
        {
            ret.failedMsg(QStringLiteral("操作失败"));
        }
    }
    ResponseUtil::replyJson(response, ret);

    QJsonObject objParam = request.getParameterJson();
    objParam.insert("access_token", "?");
    SysLog log("system", SysLog::SAVE, QStringLiteral("保存菜单信息"),
               visitUser.getUserName(),
                QString(__FILE__),
                QString(__FUNCTION__),
                GlobalFunc::JsonToString(objParam),
                request.getPeerAddress().toString(),
                request.getHeader("User-Agent"),
                QStringLiteral("保存菜单信息成功"));
    m_pHelper->insertEntity(&log);
}

//删除菜单信息
void SysMenuController::delMenu(HttpRequest& request, HttpResponse& response, SysUser& visitUser)
{
    ResultJson ret(0, true, QStringLiteral("操作成功"));
    QString idParam = request.getParameter("id");
    if(idParam.isEmpty())
    {
        ret.failedMsg(QStringLiteral("操作失败"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    int id = idParam.toInt();

    //检查是否有关联角色，不能直接删除
    QStringList condi;

    condi.clear();
    condi << "parent_id=" + id;
    QList<SysMenu> menuList;
    m_pHelper->getMenuList(menuList, condi, "");
    if(menuList.size() > 0)
    {
        ret.failedMsg(QStringLiteral("该菜单下存在子菜单，不能直接删除"));
        ResponseUtil::replyJson(response, ret);
        return;
    }

    SysMenu menu;
    menu.setId(id);
    if(!m_pHelper->deleteEntity(&menu))
    {
        ret.failedMsg(QStringLiteral("操作失败"));
    }

    ResponseUtil::replyJson(response, ret);

    QJsonObject objParam = request.getParameterJson();
    objParam.insert("access_token", "?");
    SysLog log("system", SysLog::DEL, QStringLiteral("删除菜单信息"),
               visitUser.getUserName(),
                QString(__FILE__),
                QString(__FUNCTION__),
                GlobalFunc::JsonToString(objParam),
                request.getPeerAddress().toString(),
                request.getHeader("User-Agent"),
                QStringLiteral("删除菜单信息成功"));
    m_pHelper->insertEntity(&log);
}

//获取菜单层级关系
QJsonArray SysMenuController::getMenuChildren(SysMenu& menuParent, QList<SysMenu> menuList)
{
    QJsonArray array;
    foreach(SysMenu menu, menuList)
    {
        //移除按钮
        if(menu.getType() == "1")
        {
            continue;
        }
        if(menu.getParentId() == menuParent.getId())
        {
            QJsonArray child = getMenuChildren(menu, menuList);

            QJsonObject menuobj;
            menuobj.insert("title", menu.getName());//菜单名
            menuobj.insert("name",  menu.getName());//菜单名
            menuobj.insert("spread", false);        //展开
            menuobj.insert("icon", menu.getIcon()); //菜单图标
            menuobj.insert("jump", menu.getUrl());  //菜单URL
            menuobj.insert("list", child);          //菜单下级
            array.append(menuobj);
        }
    }
    return array;
}
