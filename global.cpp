﻿#include "global.h"
#include <QJsonDocument>
#include <QCoreApplication>
#include <QDir>

TemplateCache* templateCache;
HttpSessionStore* sessionStore;
StaticFileController* staticFileController;
FileLogger* logger;


GlobalFunc::GlobalFunc()
{

}

GlobalFunc* GlobalFunc::instance(){
    return globalInstance();  // 返回线程安全的静态对象
}

QJsonObject GlobalFunc::StringToJson(QString jsonString)
{
    QJsonParseError err;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toLocal8Bit().data(), &err);
    if(err.error != QJsonParseError::NoError)
    {
        qDebug()<< "StringToJson error "<< err.errorString();
    }
    QJsonObject jsonObject = jsonDocument.object();
    return jsonObject;
}

QJsonArray GlobalFunc::StringToArray(QString jsonString)
{
    QJsonParseError err;
    QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonString.toLocal8Bit().data(), &err);
    if(err.error != QJsonParseError::NoError)
    {
        qDebug()<< "StringToArray error "<< err.errorString();
    }
    QJsonArray jsonArray = jsonDocument.array();
    return jsonArray;
}

QString GlobalFunc::JsonToString(QJsonObject jsonObject)
{
    QJsonDocument document;
    document.setObject(jsonObject);
    QByteArray simpbyte_array = document.toJson(QJsonDocument::Compact);
    QString simpjson_str = QString::fromUtf8(simpbyte_array.constData());

    return simpjson_str;
}

QString GlobalFunc::ArrayToString(QJsonArray jsonArray)
{
    QJsonDocument document;
    document.setArray(jsonArray);
    QByteArray simpbyte_array = document.toJson(QJsonDocument::Compact);
    QString simpjson_str = QString::fromUtf8(simpbyte_array.constData());
    return simpjson_str;
}


//内存绘制验证码
QString GlobalFunc::drawCaptcha(QImage& image)
{
    QString textCode = "";
    QString chrs= "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";//字符集
    int chrs_size = chrs.size();
    for(int i=0;i<4;i++)
    {
        qsrand((uint)QDateTime::currentMSecsSinceEpoch() + i);
        int randomx = qrand()%(chrs_size-1);
        QString str = chrs.at(randomx);
        textCode.append(str);
    }

    QPainter painter(&image);

    int maxPosX = image.width();
    int minPosX = 0;
    int maxPosY = image.height();
    int minPosY = 0;
    QRect rect(QPoint(minPosX,minPosY), QPoint(maxPosX,maxPosY));

    //画点
    QPen pen;
    QBrush brush;
    brush.setColor(Qt::white);
    brush.setStyle(Qt::SolidPattern);
    painter.setBrush(brush);
    painter.drawRect(rect);
    QColor color;
    pen.setWidth(3);
    pen.setStyle(Qt::DotLine);

    QPoint *points=new QPoint[500];
    for (int i=0;i<100;i++) {
        points[i]=QPoint(qrand()%(maxPosX-minPosX)+minPosX,qrand()%(maxPosY-minPosY)+minPosY);
        int R=qrand()%255;
        int G=qrand()%255;
        int B=qrand()%255;
        color.setRgb(R,G,B);
        pen.setColor(color);
        painter.setPen(pen);
        painter.drawPoint(points[i]);

    }

    QFont font;
    pen.setWidth(3);
    font.setPointSize(20);
    font.setBold(true);
    painter.setFont(font);
    //画随机字符
    for(int i=0;i<4;i++)
    {
        int R=qrand()%255;
        int G=qrand()%255;
        int B=qrand()%255;
        QColor color;
        color.setRgb(R,G,B);
        pen.setColor(color);
        painter.setPen(pen);
        QPointF pointF;
        pointF.setX((maxPosX/7+(maxPosX/4)*(i*2))/2);
        pointF.setY(maxPosY/1.5);
        painter.drawText(pointF,textCode.at(i));
    }

    return textCode;
}

QString GlobalFunc::getRandomSalt()
{
    QString chars = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    qint64 sesc = QDateTime::currentMSecsSinceEpoch();
    qsrand(sesc);
    QString salt;
    for(int i=0; i<10; i++)
    {
        int index = qrand() % chars.size();
        salt += chars.at(index);
    }
    return salt;
}

//从属性名变为字段名
//如：userName -> user_name
QString GlobalFunc::getFieldName(QString propName)
{
    for(int i=propName.length()-1; i>=1; i--)
    {
        QChar c = propName.at(i);
        if(c.isUpper())
        {
            propName.insert(i, "_");
        }
    }
    return propName.toLower();
}


QString GlobalFunc::getNowTimeString()
{
    QDateTime dt = QDateTime::currentDateTime();
    return dt.toString("yyyy-MM-dd hh:mm:ss.zzz");
}


/** Search the configuration file */
QString GlobalFunc::searchConfigDir()
{
    QString binDir=QCoreApplication::applicationDirPath();
    QString fileName("/etc/WEB.ini");

    QStringList searchList;
    searchList.append(binDir);
    searchList.append(binDir+"/..");
    searchList.append(binDir+"/../QtWebAdmin"); // for development with shadow build (Linux)
    searchList.append(binDir+"/../../QtWebAdmin"); // for development with shadow build (Windows)
    searchList.append(QDir::rootPath());

    foreach (QString dir, searchList)
    {
        QFile file(dir+"/"+fileName);
        if (file.exists())
        {
            QString absfileName=QDir(file.fileName()).canonicalPath();
            qDebug("Using config file %s",qPrintable(fileName));
            return absfileName.remove(fileName);
        }
    }

    // not found
    foreach (QString dir, searchList)
    {
        qWarning("%s/%s not found",qPrintable(dir),qPrintable(fileName));
    }
    qFatal("Cannot find config file %s",qPrintable(fileName));
    return nullptr;
}

QString GlobalFunc::loadDisk(QTemporaryFile* file)
{
    QString localFileName = "";

    return localFileName;
}
