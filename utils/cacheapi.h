﻿/*
* CacheApi 缓存接口
* 可插入键、值对，并设置有效时间，默认有效期为永久
*/

#ifndef CACHEAPI_H
#define CACHEAPI_H

#include <QMap>

class CacheApi
{
private:
    QMap<QString, QString> mapValue;
    QMap<QString, long> mapExpired;

public:
    CacheApi();
    static CacheApi *instance();

    void insert(QString key, QString value, int expiredSec);
    void remove(QString key);
    QString get(QString key);
};
Q_GLOBAL_STATIC(CacheApi, cacheInstance)


#endif // CACHEAPI_H
