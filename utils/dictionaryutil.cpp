﻿#include "dictionaryutil.h"
#include "db/sqlhelper.h"

DictionaryUtil::DictionaryUtil()
{

}

DictionaryUtil* DictionaryUtil::instance(){
    return dictInstance();  // 返回线程安全的静态对象
}


void DictionaryUtil::initDictionary()
{
    SqlHelper* pHelper = DbMgr::instance()->getHelper();
    QStringList condi;

    QList<SysDictionary> dicList;
    QList<SysDicValue> valueList;
    pHelper->getDictList(dicList, condi, "");
    pHelper->getDictValueList(valueList, condi, "value");

    m_SysDictionary.clear();

    foreach (SysDictionary dic, dicList) {
        int dicId = dic.getId();
        QString code = dic.getCode();
        QMap<QString, QString> valueMap;

        for(int i=0; i<valueList.size(); i++)
        {
            if(valueList[i].getParentId() == dicId)
            {
                QString dicKey = valueList[i].getValue();
                QString dicLabel = valueList[i].getLabel();
                valueMap.insert(dicLabel, dicKey);
            }
        }
        if(valueMap.size() > 0)
        {
            putDictionary(code, valueMap);
        }
    }
}

//插入字典
void DictionaryUtil::putDictionary(QString code, QMap<QString, QString> values)
{
    m_SysDictionary.insert(code, values);
}

//读取字典
QMap<QString, QString> DictionaryUtil::getDictionary(QString code)
{
    QMap<QString, QString> map;
    if(m_SysDictionary.contains(code))
        return m_SysDictionary[code];
    return map;
}

//读取字典值
QString DictionaryUtil::getDictionaryValue(QString code, QString dicKey)
{
    if(!m_SysDictionary.contains(code))
        return "";

    if(!m_SysDictionary[code].contains(dicKey))
        return "";
    return m_SysDictionary[code][dicKey];
}
