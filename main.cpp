﻿#include <QApplication>
#include <QWebEngineView>
#include <QWebEngineProfile>
#include <QWebEngineCookieStore>
#include <QList>
#include <QSqlDatabase>
#include <QFileInfo>
#include <QDir>

#include "global.h"
#include "httplistener.h"
#include "requestmapper.h"
#include "utils/dictionaryutil.h"
#include "utils/monitorutil.h"


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("QtWebAdmin");
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    MonitorUtil::instance()->init();

    //初始化字典
    DictionaryUtil::instance()->initDictionary();

    GlobalFunc::instance()->configDir = GlobalFunc::searchConfigDir();
    QString configFileName  =  GlobalFunc::instance()->configDir  + "/etc/WEB.ini";

    QSettings* logSettings=new QSettings(configFileName,QSettings::IniFormat,&a);
    logSettings->beginGroup("logging");
    FileLogger* logger=new FileLogger(logSettings,10000,&a);
    logger->installMsgHandler();

    // Configure template loader and cache
    QSettings* templateSettings=new QSettings(configFileName,QSettings::IniFormat,&a);
    templateSettings->beginGroup("templates");
    templateCache=new TemplateCache(templateSettings,&a);

    // Configure session store
    QSettings* sessionSettings=new QSettings(configFileName,QSettings::IniFormat,&a);
    sessionSettings->beginGroup("sessions");
    sessionStore=new HttpSessionStore(sessionSettings,&a);

    // Configure static file controller
    QSettings* fileSettings=new QSettings(configFileName,QSettings::IniFormat,&a);
    fileSettings->beginGroup("docroot");
    staticFileController=new StaticFileController(fileSettings,&a);

    // Configure and start the TCP listener
    QSettings* listenerSettings=new QSettings(configFileName,QSettings::IniFormat,&a);
    listenerSettings->beginGroup("listener");

    new HttpListener(listenerSettings,new RequestMapper(&a),&a);


//如定义了ISGUI，启动界面
#ifdef ISGUI
    QFileInfo configFile(configFileName);
    QString filePath = fileSettings->value("path",".").toString();
    filePath = QFileInfo(configFile.absolutePath(), filePath).absoluteFilePath();
    QString path = filePath + "/index.html";

    QWebEngineView view;
    view.setUrl(QUrl(path));
    view.resize(1024, 750);
    view.show();
#endif

    qWarning("Application has started");
    a.exec();
    qWarning("Application has stopped");

}



